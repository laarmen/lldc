I - Présentation
=================

Le jeu LLDC (Les Larmes Du Chaos) est (sera, grâce à vous !) un jeu médiéval-fantastique en ligne, avec tout un lore dédié.
Vous pouvez en apprendre plus sur cet univers (et contribuer à l'étendre) via le [wiki](https://gitlab.com/lldc/lldc/wikis/home).

Pour participer au projet, il suffit de suivre les étapes ci-dessous et de nous rejoindre sur IRC : #lldc@Freenode. 

II - Installation
=================

1) Clôner le projet
----------------------------------

Après avoir [ajouté votre clé SSH](https://gitlab.com/profile/keys) sur Gitlab et installé le paquet Git :

```sh
git clone git@gitlab.com:lldc/lldc.git lldc # Crée une copie locale du projet dans le dossier lldc
cd lldc

git checkout -b dev --track origin/dev # Crée la branche local "dev" à partir de la branch du serveur "dev"
                                       # le serveur cloné étant par défaut nommé "origin"
# Cette ligne est un raccourci pour :
# git branch dev --track origin/dev
# git checkout dev

php -r "readfile('https://getcomposer.org/installer');" | php # Récupère la dernière version de composer
php composer.phar install # Récupère les librairies utilisées par le projet (Symfony 3, Doctrine, Twig, etc.)
```

2) Mise en place d'un serveur local
-----------------------------------

Tout est [là](http://symfony.com/doc/current/book/installation.html).

Sur un système Debian ou dérivé (Ubuntu, Linux Mint, etc...), installer les paquets suivants :

- apache2
- PHP 7
- phpmyadmin
- mysql-client
- mysql-server
- nodejs
- npm

Ensuite le principe est le suivant (en partant de la racine du repo) :

```sh
sudo ln -s $(pwd) /var/www
mkdir -p /var/www/lldc/app/cache /var/www/lldc/app/logs
chmod -R 777 app/cache app/logs /var/www/lldc/app/cache /var/www/lldc/app/logs
if [ -d /etc/apache2/conf-available ]; then
    sudo cp apache_dev.conf /etc/apache2/conf-available/lldc.conf
    sudo a2enconf lldc
else
    sudo cp apache_dev.conf /etc/apache2/conf.d/lldc.conf
fi
sudo service apache2 reload
```

L'installation de `less`, `uglifyjs` et `uglifycss` nécessite `nodejs` et `npm`.

```sh
sudo npm install -g less
sudo npm install -g uglify-js
sudo npm install -g uglifycss
```

Après ça, [http://localhost:1337/config.php](http://localhost:1337/config.php) et suivez les instructions.
Exemple de paramètres à utiliser pour la base de données : `Base = lldc` / `User = lldc` / `Password = lldc`.
Pour configurer les paramètres node_*, vous pouvez regarder le fichier `parameters.dist.yml`, qui présente la configuration classique sous Debian.


Si vous êtes sous un autre système, n'oubliez simplement pas de créer les dossiers `app/cache` et `app/logs`, et débrouillez-vous pour le reste.

Modifiez également votre fichier php.ini, en configurant le paramètre `memory_limit` à 256.

Une fois votre serveur configuré et fonctionnel, vous pouvez vous occuper de la compilation du LESS en CSS en un fichier main.css dans web/bundles/lldc/css/compiled/ ainsi que de la minification des fichiers Javascript

```sh
php bin/console assetic:dump
```

3) Mise en place de la base de données
-----------------------------------

a - Base de développement 

Exemple de paramètres à utiliser : Base = lldc / User = lldc / Password = lldc
Après avoir créé votre base de données sur votre serveur de bases de données, copiez le fichier `/app/config/parameters.yml.dist` vers `/app/config/parameters.yml`, puis configurez le.
Lancez ensuite la commande suivante :

```sh
php bin/console doctrine:schema:create
```

Cette commande va générer les tables directement dans la base de données configurée dans `/app/config/parameters.yml`.
Lancez ensuite la commande suivante pour initialiser la base (création de la partie principale dans la table `Game`, des races dans `Race`, etc.) :

```sh
php bin/console lldc:init-db
```

Voilà, la base de développement est prête.

b - Base de test

Les mêmes étapes sont nécessaires afin de mettre en place une base de données de tests.
Pour cela, créez votre base de données, de la même manière que pour votre base de données de développement.
Exemple de paramètres à utiliser : Base = lldc_test / User = lldc_test / Password = lldc_test
Après avoir créer votre base de données, copiez le fichier `/app/config/parameters.yml` vers `/app/config/parameters_test.yml`, puis configurez le avec les identifiants "kivonbien".

Lancez ensuite les commandes suivantes (création puis initialisation) :

```sh
php bin/console doctrine:schema:create -e test
php bin/console lldc:init-db -e test
```

4) Mise en place de l'automatisation des tâches
-----------------------------------

Afin de faire tourner correctement le site sur votre machine, vous devez mettre en place les tâches planifiées (production de ressources, déroulement des combats...)
Pour cela, lancez tout d'abord la commande suivante :

```sh
php bin/console lldc:generate:crontab
```

Cette commande génère des lignes qu'il vous suffit de mettre dans votre crontab 

```sh
crontab -e
```

À chaque mise à jour, supprimez les lignes correspondant à LLDC de votre crontab avant d'ajouter les nouvelles !

5) Liens utiles
-----------------------------------

Installation de nodejs, npm, less :

- http://devyourdream.net/2012/09/20/symfony2-et-assetic-comment-integrer-less-et-yui-compressor-sur-ubuntu-linux/
- http://pirmax.fr/2014/05/07/installation-nodejs-npm-debian-stable/

(Notez que le dernier lien n'est utile que si nodejs n'est pas disponible dans votre distribution, ce qui est rare)

III - Développement
=================

1) Développement en utilisant GIT
----------------------------------

a - Environnement de MR afin de tester le code dans un environnement dédié (facultatif)

Les mêmes étapes que pour la création d'une base de données de test sont nécessaires afin de mettre en place une base de données pour l'environnement de MR.
Cet environnement permet simplement d'avoir une base dédiée aux tests des différentes MR avant de les valider.
Il vous faut simplement copier le fichier `/app/config/parameters.yml` vers `/app/config/parameters_mr.yml` et le configurer correctement.
Puis, création/initialisation de la base de données dédiée :

```sh
php bin/console doctrine:schema:create -e mr
php bin/console lldc:init-db -e mr
```

L'accès à cet environnement se fait via l'URL [http://localhost:1337/app_mr.php](http://localhost:1337/app_mr.php).

Un script d'accès et d'initialisation, écrit en Python3 (il faut donc qu'il soit installé)
est présent à `scripts/mr.py`. Ses dépendances, (noms de paquets Debian) sont :

  * python3
  * python3-yaml

Pour savoir ce qu'il fait, exécutez

```sh
./scripts/mr.py --help
```

b - Processus complet d'une mise à jour

Pour apprendre GIT, n'hésitez pas à lire ce book, très complet : http://git-scm.com/book

Tout d'abord, il faut créer son propre fork. Pour ce faire, allez sur la page
principale du projet et cherchez le bouton idoine. Une fois le fork effectué,
allez dans les préférences (settings) du fork, et mettez-le en visibilité
"Private" si vous voulez que les gens puissent lire votre code au fur et à
mesure (le workflow "Internal" ouvre à tout gitlab.com). Il faut ensuite 
aller, toujours dans Settings, dans l'onglet "Groups", et donner l'accès 
'Reporter' au groupe LLDC.

Ajoutez ensuite votre fork à votre copie locale du répo git:

```sh
git remote add $PSEUDO git@gitlab.com:$PSEUDO/lldc.git # Notre convention est de garder origin pour le répo principal
git fetch $PSEUDO
```

Les développeurs créent une branche par développement à faire (par exemple,
ajout d'une fonctionnalité, ou correction du bug #42) à partir de la branche
`origin/dev`:

```sh
git fetch origin # Mettre à jour origin, au cas où.
git checkout -b fix/bug42 origin/dev # Toujours se baser sur origin/dev, ça évite les conflits à la con.
                                        # Ne PAS mettre --track, c'est comme ça que les accidents bêtes arrivent.
git push -u $PSEUDO fix/bug42 # crée la branche sur le fork, et informe git que les push se font sur le fork.
```

Le travail s'effectue sur cette branche. Une fois terminé, push de la branche sur le serveur GIT (origin):

```sh
git push
```

Il faut ensuite créer une *merge request*, qui sera prise en charge par un
autre développeur ayant pour rôle de relire et *tester* le code (ce qui ne
dispense pas de le faire soi-même).

Pour ce faire, le relecteur doit ajouter votre fork en tant que nouvelle remote
si cela n'a pas déjà été fait :

```sh
git remote add $PSEUDO git@gitlab.com:$PSEUDO/lldc.git # Ça sera en lecture seule, z'inquiétez pas.
git fetch $PSEUDO
```

Pour accéder au code, il suffit de faire

```sh
git checkout $PSEUDO/fix/42
# ou
git log -p $PSEUDO/fix/42
```
La première permet de tester le code, la seconde permet de relire les commits un par un pour
s'assurer qu'on garde un historique pas trop dégueu.

Si tout va bien, alors, il merge le code
avec la branche `dev` :

```sh
git fetch origin
git checkout dev
git merge origin/dev # Au cas où ta version de dev serait pas à jour
git merge $PSEUDO/fix/bug42 --no-ff # Le --no-ff crée un commit de merge même quand c'est linéaire, ça permet d'avoir
                                      # facilement un historique des merges sur dev
git push origin dev
```

Aide :

```sh
git log -p origin/pseudo/bug42 # Pour vérifier les changements`
```

Une nouvelle *merge request* est créée et assignée au *master*, qui devra à son tour merger `dev` avec `master`. Le code peut alors être mis en production.

2) Utilisations des tests unitaires
-----------------------------------

Le Bundle de LLDC comporte des tests unitaires.
Ils sont utilisables avec le framework php : phpunit.
Ce framework est installé avec composer lorsque vous lancez l'installation des dépendances du Bundle.
Si vous ne voyez pas le framework phpunit dans le dossier bin [consultez le manuel d'installation du framework](http://phpunit.de/manual/current/en/installation.html#installation.composer)

Voici la commande pour exécuter les tests unitaires :
```sh
bin/phpunit -c app/
```

Pour définir et utiliser des tests unitaires dans le Bundle de LLDC, je vous suggère de faire un tour sur le [book de symfony à ce sujet](http://symfony.com/fr/doc/current/book/testing.html).

3) Symfony
-----------------------------------

[Book Symfony, à lire, vraiment :)](http://symfony.com/doc/current/book/index.html)

[API Symfony, une fois qu'on est plongé dans le dev](http://api.symfony.com/3.1/classes.html)

[Normes de codage](http://symfony.com/doc/current/contributing/code/standards.html)
