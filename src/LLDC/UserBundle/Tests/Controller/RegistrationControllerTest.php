<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\UserBundle\Tests\Controller
 */
namespace LLDC\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegistrationControllerTest extends WebTestCase {

    private $client;
    private $container;

    public function setUp() {
        $this->client = static::createClient();

        $kernel = static::createKernel();
        $kernel->boot();
        $this->container = $kernel->getContainer();
    }

    /**
     * Tests the registration workflow
     */
    public function testRegister() {

        // Default page, with the registration form
        $crawler = $this->client->request('GET', '/');

        // Get the registration form
        $form = $crawler->selectButton('S\'inscrire !')->form();

        // Set good values
        $form['fos_user_registration_form[username]'] = 'Phpunit';
        $form['fos_user_registration_form[registrationRealmName]'] = 'Phpunit realm';
        $form['fos_user_registration_form[email]'] = 'phpunit@lldc.fr';
        $form['fos_user_registration_form[plainPassword][first]'] = 'phpunitpass';
        $form['fos_user_registration_form[plainPassword][second]'] = 'phpunitpass';
        $form['fos_user_registration_form[registrationGender]'] = 'male';
        $form['fos_user_registration_form[registrationRace]'] = 'humans';
        $form['fos_user_registration_form[registrationIdGuild]'] = 0;

        // Submit the form
        $crawler = $this->client->submit($form);

        // Check if the response status is successful
        $this->assertTrue($this->client->getResponse()->isRedirect('/check-email'));

        // Retrieve the user created
        $user = $this->container->get('doctrine')->getRepository('LLDCBundle:User')->findOneByUsername('Phpunit');

        // Check if the user is not null
        $this->assertNotNull($user);

        // Following the link sent in the confirmation email
        $crawler = $this->client->request('GET', '/confirm/'.$user->getConfirmationToken());

        // This assertion tests the title's content. If it's not a redirection to /confirmed, then, the error is displayed. It allows us to debug easily.
        $this->assertEquals('Redirecting to /confirmed', $crawler->filter('title')->text());

        /*
         * Rollback all modifications on the database by deleting the user
         */
        $service = $this->container->get('lldc.user');
        $service->deleteUser($user);
    }
}
