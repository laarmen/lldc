<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace LLDC\UserBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller managing the registration
 */
class RegistrationController extends BaseController
{
    public function registerAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);
        $user->setLocale('fr');
        $user->setDateLastAction(new \Datetime());

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            //-------- OVERRIDING FROM HERE
            $ok = false;

            /**
             * Form validation
             */
            $data = $form->getData();

            $race = array_key_exists($data->getRegistrationRace(), $this->container->getParameter('lldc')['races']) ? $data->getRegistrationRace() : 0;
            $guild = $data->getRegistrationIdGuild() > 0 ? $data->getRegistrationIdGuild() : 0;
            $gender = $data->getRegistrationGender() > 0 ? $data->getRegistrationGender() : 0;

            /**
             * Username
             */
            if($data->getUsername() === "") {
                $this->container->get('session')->getFlashBag()->add('error', $this->container->get('translator')->trans('registration.username.empty'));
            }
            elseif($userManager->findUserByUsername($data->getUsername()) != null) {
                $this->container->get('session')->getFlashBag()->add('error', $this->container->get('translator')->trans('registration.username.already-used'));
            }

            /**
             * RegistrationRealmName
             */
            elseif($data->getRegistrationRealmName() === "") {
                $this->container->get('session')->getFlashBag()->add('error', $this->container->get('translator')->trans('registration.realmName.empty'));
            }

            /**
             * Password
             
            elseif($data['plainPassword']['first'] != $data['plainPassword']['second']) {
                $this->container->get('session')->getFlashBag()->add('error', $this->container->get('translator')->trans('registration.password.not-equal'));
            }*/

            /**
             * Email
             */
            elseif($data->getEmail() === "" || !filter_var($data->getEmail(), FILTER_VALIDATE_EMAIL)) {
                $this->container->get('session')->getFlashBag()->add('error', $this->container->get('translator')->trans('registration.email.invalid'));
            }
            elseif($userManager->findUserByEmail($data->getEmail()) != null) {
                $this->container->get('session')->getFlashBag()->add('error', $this->container->get('translator')->trans('registration.email.already-used'));
            }

            /**
             * Race
             */
            elseif(!array_key_exists($race, $this->container->getParameter('lldc')['races'])) {
                $this->container->get('session')->getFlashBag()->add('error', $this->container->get('translator')->trans('registration.race.empty'));
            }

            /**
             * Gender
             */
            elseif($gender!='male' && $gender!='female') {
                $this->container->get('session')->getFlashBag()->add('error', $this->container->get('translator')->trans('registration.gender.empty'));
            }

            /**
             * Guild
             */
            else {
                $ok = true;
            }

            if ($ok) {
            //------- TO HERE
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->container->get('router')->generate('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

                return $response;
            }
        }

        return $this->render('FOSUserBundle:Registration:register.html.twig', array(
            'form' => $form->createView(),
            'races' => $this->getParameter('lldc')['races']
        ));
    }

    /**
     * Receive the confirmation token from user email provider, login the user
     */
    public function confirmAction(Request $request, $token)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');

        $user->setConfirmationToken(null);
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, $event);

        $userManager->updateUser($user);

        if (null === $response = $event->getResponse()) {
            $url = $this->container->get('router')->generate('fos_user_registration_confirmed');
            $response = new RedirectResponse($url);
        }

        //-------- OVERRIDING FROM HERE
        $this->container->get('lldc.user')->createUser($user);
        //-------- TO HERE

        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, new FilterUserResponseEvent($user, $request, $response));

        return $response;
    }
}
