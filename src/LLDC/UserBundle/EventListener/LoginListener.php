<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace LLDC\UserBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\UserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use LLDC\Bundle\Util\Util;

class LoginListener implements EventSubscriberInterface
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => 'onImplicitLogin',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
        );
    }

    public function onImplicitLogin(UserEvent $event)
    {
        $this->loadSession();
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $this->loadSession();
    }

    public function loadSession()
    {
        $doctrine = $this->container->get('doctrine');
        $trans = $this->container->get('translator');

        $token = $this->container->get('security.token_storage')->getToken();

        $game = $doctrine->getRepository('LLDCBundle:Game')->findOneByMain(1);
        $realm = $doctrine->getRepository('LLDCBundle:Realm')->findBy(array('game' => $game, 'user' => $token->getUser()));

        /**
         * $realm[0] define the realm where we send the user after his connection.
         */
        if(isSet($realm[0])) {
            $race = $realm[0]->getCharacter()->getRace()->getLabel();
            $this->container->get('session')->set('_locale', $token->getUser()->getLocale());
            $this->container->get('translator')->setLocale($token->getUser()->getLocale());

            $menu = array();
            foreach(Util::getMenuKeys() as $m) {
                $menu[$m]['label'] = $trans->trans('menu.'.$race.'.'.$m.'.label', array(), 'menus');
                $menu[$m]['description'] = $trans->trans('menu.'.$race.'.'.$m.'.description', array(), 'menus');
            }

            $this->container->get('session')->set('game', $game);
            $this->container->get('session')->set('menu', $menu);
            $this->container->get('session')->set('race', $race);
        }
    }
}
