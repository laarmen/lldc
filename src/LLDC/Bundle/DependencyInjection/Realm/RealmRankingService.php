<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\DependencyInjection\Realm
 */
namespace LLDC\Bundle\DependencyInjection\Realm;

use LLDC\Bundle\DependencyInjection\Service as Service;
use LLDC\Bundle\LLDCException;

use LLDC\Bundle\Entity\Game;
use LLDC\Bundle\Entity\Realm;

/**
 * Provides methods acting on the realm's ranking.
 */
class RealmRankingService extends Service {

    /**
    * Update the value field for the points ranking
    */
    public function updatePoints(Realm $realm) {
        $ranking = $this->getRepository('LLDCBundle:RealmRanking')->findOneBy(array('realm' => $realm, 'type' => 'points'));

        $points = $this->getPoints($realm);

        $ranking->setValue($points['total']);

        return true;
    }

    public function getPoints(Realm $realm) {
        $lldc = $this->getLLDC();
        $points = array();
        $points['total'] = 0;
        /**
        * The units db structure is too complicated to get points in a single DQL query.
        * TODO : Handle this block in a single query if possible
        */
        $points['units'] = array();

        foreach($lldc['units'][$realm->getCharacter()->getRace()->getLabel()] as $type => $unit) {

            $points['units'][$type] =
                $lldc['units'][$realm->getCharacter()->getRace()->getLabel()][$type]['points']
                *
                $this->getRepository('LLDCBundle:Troop')->findSumByRealmAndTypeAsTotal($realm, $type)['total'];

            $points['total'] += $points['units'][$type];
        }

        $points['resources'] = $this->getRepository('LLDCBundle:RealmRanking')->calculatePoints($realm, $lldc['global']['resources']['points']);

        foreach($points['resources'] as $type => $value) {
            $points['total'] += $value;
        }

        return $points;
    }

    public function updatePointsPosition(Game $game) {
        $this->getRepository('LLDCBundle:RealmRanking')->updatePointsPosition($game);
        return true;
    }

}
