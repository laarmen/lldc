<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\DependencyInjection\Realm
 */
namespace LLDC\Bundle\DependencyInjection\Realm;

use LLDC\Bundle\DependencyInjection\Service as Service;

use LLDC\Bundle\Entity\Realm;
use LLDC\Bundle\Entity\RealmBuilding;
use LLDC\Bundle\Entity\OngoingBuilding;

use LLDC\Bundle\LLDCEvents;
use LLDC\Bundle\Event\BuildingEvent;

use LLDC\Bundle\Util\Util;
use LLDC\Bundle\Util\ResourcesBean;

/**
 * Provides methods acting on the RealmBuilding and OngoingBuilding entities.
 * Allows you to start construction...
 */
class BuildingsService extends Service {

    /**
     * This method only adds the given buildings to the ongoing buildings of the realm.
     * * <b>No verification</b>
     *
     * @param RealmBuilding $realmBuilding
     * @param Realm $realm
     */
    private function startBuilding(RealmBuilding $realmBuilding, Realm $realm)
    {
        // Retrieve the LLDC's configuration
        $lldc = $this->getLLDC();
        $now = new \DateTime();

        // We get the ratio for this realm (using the race and the game we're on)
        $ratio = $lldc['races'][$realm->getCharacter()->getRace()->getLabel()]['buildings']['speed'] * $realm->getGame()->getBuildingsSpeed();

        // Basic delay for this building
        $delay = $lldc['buildings'][$realm->getCharacter()->getRace()->getLabel()][$realmBuilding->getType()]['cost']['delay'] * $ratio;
        for($i = 0; $i < $realmBuilding->getAmount(); $i++) {
            $ongoingBuilding = new OngoingBuilding();
            $ongoingBuilding->setType($realmBuilding->getType());
            $ongoingBuilding->setRealm($realm);
            $ongoingBuilding->setDateBegin($now);
            $ongoingBuilding->setDateLastWork($ongoingBuilding->getDateBegin());
            $ongoingBuilding->setDuration($delay);
    
            $this->getContainer()->get('doctrine')->getEntityManager()->persist($ongoingBuilding);
        }

        return true;
    }

    /**
     * This method buys the given buildings for the current realm/game.
     *
     * @param RealmBuilding $realmBuilding - Buildings to buy
     * @param Realm $realm - If given, buy the units for this realm, otherwise, the realm is retrived with the user/game in session - <b>Default : null</b>
     *
     * @throws Exception - Not enough resources
     *
     * @return true
     */
    public function buyBuildings(RealmBuilding $realmBuilding, Realm $realm = null)
    {
        // Get the resources service
        $resources = $this->getContainer()->get('lldc.resources');

        if($realm == null) {
            $realm = $this->getRealm();
        }

        // Calculate the costs of the given buildings
        $costs = $this->getCosts($realmBuilding, $realm);

        // Take resources from the RealmResources entity, using the resources service
        $resources->take($costs);

        // Add the buildings to the realmBuilding entity
        $this->startBuilding($realmBuilding, $realm);

        // Persist the modifications
        $this->getContainer()->get('doctrine')->getEntityManager()->flush();

        return true;
    }
    
    /**
     * This method calculates the cost of the given buildings.
     *
     * @param realmBuilding $realmBuilding - List of buildings
     * @param Realm $realm - The realm for which we wanna calculate the cost
     *
     * @return ResourcesBean
     */
    public function getCosts(RealmBuilding $realmBuilding, Realm $realm = null)
    {
        // Retrieve the LLDC's configuration
        $lldc = $this->getLLDC();

        // Cost's ratio for the current game
        $ratio = $realm->getGame()->getBuildingsCost();

        // Fetching costs
        $cost = $lldc['buildings'][$realm->getCharacter()->getRace()->getLabel()][$realmBuilding->getType()]['cost']['resources'];

        $resources = new ResourcesBean();

        // Calculating resources needed
        $resources->setFood($realmBuilding->getAmount()*$cost['food']*$ratio);
        $resources->setWood($realmBuilding->getAmount()*$cost['wood']*$ratio);
        $resources->setGold($realmBuilding->getAmount()*$cost['gold']*$ratio);
        $resources->setRock($realmBuilding->getAmount()*$cost['rock']*$ratio);
        $resources->setMadrens($realmBuilding->getAmount()*$cost['madrens']*$ratio);

        // Return the resources needed
        return $resources;
    }
}
