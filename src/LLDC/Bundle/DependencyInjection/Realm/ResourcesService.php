<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\DependencyInjection\Realm
 */
namespace LLDC\Bundle\DependencyInjection\Realm;

use LLDC\Bundle\DependencyInjection\Service as Service;
use LLDC\Bundle\LLDCException;

use LLDC\Bundle\Entity\Realm;

use LLDC\Bundle\Util\ResourcesBean;

/**
 * Provides methods acting on the realm's resources.
 */
class ResourcesService extends Service {

    /**
     * @see changeMorale()
     */
    const MORALE_DIRECT_VALUE = 1;
    /**
     * @see changeMorale()
     */
    const MORALE_PERCENTAGE = 2;

    /**
     * Takes resources given from the realm given
     * 
     * @param ResourcesBean $resources
     * @param Realm $realm = null - If given, take the resources from this realm, otherwise, the realm is retrived with the user/game in session
     */
    public function take(ResourcesBean $resources, Realm $realm = null)
    {
        // Fetch the current realm if needed
        if(is_null($realm)) {
            $realm = $this->getRepository('LLDCBundle:Realm')->findOneBy(array('game' => $this->getGame(), 'user' => $this->getUser()));
        }

        // We can't take negative resources
        if($resources->getFood() < 0) {
            $resources->setFood(0);
        }
        if($resources->getWood() < 0) {
            $resources->setWood(0);
        }
        if($resources->getGold() < 0) {
            $resources->setGold(0);
        }
        if($resources->getRock() < 0) {
            $resources->setRock(0);
        }
        if($resources->getMadrens() < 0) {
            $resources->setMadrens(0);
        }
        if($resources->getXp() < 0) {
            $resources->setXp(0);
        }

        // If the user has enough resources, it passes. Otherwise, it throws an Exception
        $this->hasEnough($resources, $realm);

        $realm->getResource()->setFood($realm->getResource()->getFood()-$resources->getFood());
        $realm->getResource()->setWood($realm->getResource()->getWood()-$resources->getWood());
        $realm->getResource()->setGold($realm->getResource()->getGold()-$resources->getGold());
        $realm->getResource()->setRock($realm->getResource()->getRock()-$resources->getRock());
        $realm->getResource()->setMadrens($realm->getResource()->getMadrens()-$resources->getMadrens());
        $realm->getResource()->setXp($realm->getResource()->getXp()-$resources->getXp());

        $this->getContainer()->get('lldc.ranking')->updatePoints($realm);
        $this->getContainer()->get('lldc.ranking')->updatePointsPosition($realm->getGame());

        return true;
    }

    /**
     * Returns true if the user has enough resources
     * Else, throws an Exception
     * 
     * @param ResourcesBean $resources
     */
    public function hasEnough(ResourcesBean $resources, Realm $realm = null)
    {
        if(is_null($realm)) {
            $realm = $this->getRepository('LLDCBundle:Realm')->findOneByUser($this->getUser()->getId());
        }

        if($resources->getFood() > 0)
        {
            if($realm->getResource()->getFood() <= $resources->getFood())
            {
                throw new LLDCException($this->getContainer()->get('translator')->trans('realm.resources.not.enough.food'), LLDCException::NOT_ENOUGH_FOOD);
            }
        }
        if($resources->getWood() > 0)
        {
            if($realm->getResource()->getWood() <= $resources->getWood())
            {
                throw new LLDCException($this->getContainer()->get('translator')->trans('realm.resources.not.enough.wood'), LLDCException::NOT_ENOUGH_WOOD);
            }
        }
        if($resources->getGold() > 0)
        {
            if($realm->getResource()->getGold() <= $resources->getGold())
            {
                throw new LLDCException($this->getContainer()->get('translator')->trans('realm.resources.not.enough.gold'), LLDCException::NOT_ENOUGH_GOLD);
            }
        }
        if($resources->getRock() > 0)
        {
            if($realm->getResource()->getRock() <= $resources->getRock())
            {
                throw new LLDCException($this->getContainer()->get('translator')->trans('realm.resources.not.enough.rock'), LLDCException::NOT_ENOUGH_ROCK);
            }
        }
        if($resources->getMadrens() > 0)
        {
            if($realm->getResource()->getMadrens() <= $resources->getMadrens())
            {
                throw new LLDCException($this->getContainer()->get('translator')->trans('realm.resources.not.enough.madrens'), LLDCException::NOT_ENOUGH_MADRENS);
            }
        }
        if($resources->getXp() > 0)
        {
            if($realm->getResource()->getXp() <= $resources->getXp())
            {
                throw new LLDCException($this->getContainer()->get('translator')->trans('realm.resources.not.enough.xp'), LLDCException::NOT_ENOUGH_XP);
            }
        }

        return true;
    }

    /**
     * The method will change the morale of the given realm.
     * @param $value The value
     * @param $realm Realm
     * @param $type ResourcesService::MORALE_DIRECT_VALUE or ResourcesService::MORALE_PERCENTAGE
     *  MORALE_DIRECT_VALUE :
     *      With the value you give.
     *      Example - morale = 80
     *      changeMorale(10, MORALE_DIRECT_VALUE) returns 90
     *  MORALE_PERCENTAGE :
     *      By calculating the percentage with the value you give.
     *      Example - morale = 80
     *      changeMorale(10, MORALE_PERCENTAGE) returns 88
     *
     * To add morale, pass a positive $value.
     * To substract morale, pass a negative $value.
     *
     * @return double
     */
    public function changeMorale($value, Realm $realm = null, $type = self::MORALE_DIRECT_VALUE) {
        if(is_null($realm)) {
            $realm = $this->getRepository('LLDCBundle:Realm')->findOneByUser($this->getUser()->getId());
        }

        if($type===self::MORALE_DIRECT_VALUE) {
            $morale = $realm->getResource()->getMorale() + $value;
        }
        elseif($type===self::MORALE_PERCENTAGE) {
            $morale = $realm->getResource()->getMorale() * (1+$value/100);
        }
        else {
            throw new LLDCException('Wrong $type parameter. MORALE_DIRECT_VALUE or MORALE_PERCENTAGE expected.');
        }

        $lldc = $this->getLLDC();

        $morale = round($morale, 2);
        if($morale > $lldc['morale']['max']) {
            $morale = $lldc['morale']['max'];
        }
        elseif($morale < $lldc['morale']['min']) {
            $morale = $lldc['morale']['min'];
        }

        $realm->getResource()->setMorale($morale);

        return $morale;
    }

    /**
     * Returns a string representing the morale
     *
     * @param float $morale The morale
     * @return string
     * TODO i18n
     */
    public function getMoraleHumanized($morale) {
        if($morale <= 85) {
            $moraleHumanized = 'très faible';
        }
        elseif($morale < 90) {
            $moraleHumanized = 'faible';
        }
        elseif($morale < 95) {
            $moraleHumanized = 'moyen';
        }
        elseif($morale < 100) {
            $moraleHumanized = 'bon';
        }
        elseif($morale < 105) {
            $moraleHumanized = 'très bon';
        }
        elseif($morale <= 110) {
            $moraleHumanized = 'excellent';
        }
        else {
            throw LLDCException('The given $morale can\'t be humanized.');
        }
        return $moraleHumanized;
    }

}
