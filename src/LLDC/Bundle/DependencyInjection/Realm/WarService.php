<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\DependencyInjection\Realm
 */
namespace LLDC\Bundle\DependencyInjection\Realm;

use LLDC\Bundle\DependencyInjection\Service as Service;

use LLDC\Bundle\Entity\Realm;
use LLDC\Bundle\Entity\Troop;
use LLDC\Bundle\Entity\War;

/**
 * Provides methods acting on the wars.
 */
class WarService extends Service {

    /**
     * Starts a war between two realms
     */
    public function launchAttack(Realm $attacker, Realm $defender) {
        $war = new War();
        $war->setDateCreation(new \DateTime());
        $this->getManager()->persist($war);

        $attacker->addAttacking($war);
        $defender->addDefending($war);

        $war->setBattlefield($this->getRepository('LLDCBundle:Battlefield')->findOneBy(array('place' => $defender->getPlace())));
        if(is_null($war->getBattlefield())) {
            throw new \Exception('No battlefield found.');
        }

        // With all available troops but peon
        $sentTroop = 0;
        foreach($this->getRepository('LLDCBundle:Troop')->findBy(array('realm' => $attacker, 'war' => null)) as $troop) {
            if($troop->getType()!='peon') {
                $troop->setWar($war);
                $sentTroop++;
            }
        }

        if($sentTroop == 0) {
            throw new \Exception('No troop to send.');
        }
    }

}
