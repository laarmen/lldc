<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\DependencyInjection
 */
namespace LLDC\Bundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * All services extend this abstract class.
 * It provides useful shortcuts
 */
abstract class Service {

    /**
     * @var Container The container provides doctrine, translator, etc.
     */
    private $container;
    
    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }
    
    /**
     * Returns the current user
     * @return \LLDC\Bundle\Entity\User The current user, from the security token
     */
    protected function getUser()
    {
        return $this->getContainer()->get('security.token_storage')->getToken()->getUser();
    }
    
    /**
     * Returns the container (which provides for example Doctrine or the event dispatcher)
     * @return Container The container
     */
    protected function getContainer()
    {
        return $this->container;
    }

    /**
     * Gets the repository for a class.
     * @param string $className
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRepository($className) {
        return $this->getContainer()->get('doctrine')->getRepository($className);
    }

    /**
     * Returns the entity manager
     * @return \Symfony\Bundle\DoctrineBundle\Registry The Doctrine Registry service
     */
    protected function getManager() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Returns the current game
     * @return \LLDC\Bundle\Entity\Game The current game
     */
    protected function getGame()
    {
        return 1; //$this->getContext()->getToken()->getAttribute('game');
    }

    /**
     * Returns the current realm
     * @return \LLDC\Bundle\Entity\Realm The current user's realm
     */
    protected function getRealm()
    {
        return $this->getContainer()->get('doctrine')->getRepository('LLDCBundle:Realm')->findOneBy(array('game' => $this->getGame(), 'user' => $this->getUser()));
    }

    /**
     * Returns the LLDC's configuration
     * @return array(mixed) The LLDC's configuration
     */
    protected function getLLDC() {
        return $this->getContainer()->getParameter('lldc');
    }

    /**
     * Returns the event dispatcher
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected function getEventDispatcher() {
        return $this->getContainer()->get('event_dispatcher');
    }
}
