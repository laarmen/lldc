<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Util
 */
namespace LLDC\Bundle\Util;

/**
 * This class provides some util static methods
 */
class Util {
    /**
     * @var array(string) $buildingsKeys List of all types of buildings
     */
    private static $buildingsKeys = array('house', 'spycenter', 'market', 'barrack', 'archery', 'stable', 'temple', 'special');
    /**
     * @var array(string) $menuKeys List of all menus id
     */
    private static $menuKeys = array(
        'profile', 'amneroth', 'help', 'trombino', 'history', 'tutorial', 'ranking',
        'realm', 'academy', 'temple', 'research', 'market', 'war', 'secret', 'hero', 'library', 'guild', 'messenger', 'other');

    /**
     * This method returns the list of all types of buildings
     *
     * @return array(string) List of all types of buildings
     */
    public static function getBuildingsKeys()
    {
        return self::$buildingsKeys;
    }

    /**
     * This method returns the list of all menus id
     *
     * @return array(string) List of all menus id
     */
    public static function getMenuKeys()
    {
        return self::$menuKeys;
    }
}
