<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Util
 */
namespace LLDC\Bundle\Util;

/**
 * This bean wraps the resources
 */
class ResearchBean
{
    /**
     * @var string
     */
    private $part;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $food;
    /**
     * @var int
     */
    private $wood;
    /**
     * @var int
     */
    private $rock;
    /**
     * @var int
     */
    private $gold;
    /**
     * @var int
     */
    private $madrens;
    /**
     * @var int
     */
    private $xp;
    /**
     * @var int
     */
    private $points;
    /**
     * @var int
     */
    private $duration;
    /**
     * @var int
     */
    private $value;
    /**
     * @var array(String)
     */
    private $require;
    /**
     * @var array(String)
     */
    private $successor = array();
    /**
     * @var OngoingResearch
     */
    private $ongoing;

    /**
     * Constructor
     *
     * @param int $food
     * @param int $wood
     * @param int $rock
     * @param int $gold
     * @param int $madrens
     */
    public function __construct(
        $part = '', $type = '', $name = '', 
        $food = 0, $wood = 0, $rock = 0, $gold = 0, $madrens = 0, $xp = 0, $points = 0, 
        $duration = 0, $value = 0, $require = array())
    {
        $this->setPart($part);
        $this->setType($type);
        $this->setName($name);
        $this->setFood($food);
        $this->setWood($wood);
        $this->setRock($rock);
        $this->setGold($gold);
        $this->setMadrens($madrens);
        $this->setXp($xp);
        $this->setPoints($points);
        $this->setDuration($duration);
        $this->setValue($value);
        $this->setRequire($require);
        $this->setOngoing(null);
    }

    /**
     * Return the part
     *
     * @return part
     */
    public function getPart()
    {
        return $this->part;
    }
    /**
     * Set the part
     *
     * @param int $part
     */
    public function setPart($part)
    {
        $this->part = $part;
    }

    /**
     * Return the type
     *
     * @return type
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set the type
     *
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Return the name
     *
     * @return name
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set the name
     *
     * @param int $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Return the full name of the research
     *
     * @return String
     */
    public function getFullname() {
        return $this->getPart().'.'.$this->getType().'.'.$this->getName();
    }

    /**
     * Return the food
     *
     * @return food
     */
    public function getFood()
    {
        return $this->food;
    }
    /**
     * Set the food
     *
     * @param int $food
     */
    public function setFood($food)
    {
        $this->food = $food;
    }

    /**
     * Return the wood
     *
     * @return wood
     */
    public function getWood()
    {
        return $this->wood;
    }
    /**
     * Set the wood
     *
     * @param int $wood
     */
    public function setWood($wood)
    {
        $this->wood = $wood;
    }

    /**
     * Return the rock
     * 
     * @return rock
     */
    public function getRock()
    {
        return $this->rock;
    }
    /**
     * Set the rock
     * 
     * @param int $rock
     */
    public function setRock($rock)
    {
        $this->rock = $rock;
    }
    
    /**
     * Return the gold
     * 
     * @return gold
     */
    public function getGold()
    {
        return $this->gold;
    }
    /**
     * Set the gold
     * 
     * @param int $gold
     */
    public function setGold($gold)
    {
        $this->gold = $gold;
    }
    
    /**
     * Return the madrens
     * 
     * @return madrens
     */
    public function getMadrens()
    {
        return $this->madrens;
    }
    /**
     * Set the madrens
     * 
     * @param int $madrens
     */
    public function setMadrens($madrens)
    {
        $this->madrens = $madrens;
    }
    
    /**
     * Return the xp
     * 
     * @return xp
     */
    public function getXp()
    {
        return $this->xp;
    }
    /**
     * Set the xp
     * 
     * @param int $xp
     */
    public function setXp($xp)
    {
        $this->xp = $xp;
    }
    
    /**
     * Return the points
     * 
     * @return points
     */
    public function getPoints()
    {
        return $this->points;
    }
    /**
     * Set the points
     * 
     * @param int $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }
    
    /**
     * Return the duration
     * 
     * @return duration
     */
    public function getDuration()
    {
        return $this->duration;
    }
    /**
     * Set the duration
     * 
     * @param int $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }
    
    /**
     * Return the value
     * 
     * @return value
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Set the value
     * 
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Return the require
     * 
     * @return require
     */
    public function getRequire()
    {
        return $this->require;
    }
    /**
     * Set the require
     * 
     * @param array $require
     */
    public function setRequire($require)
    {
        $this->require = $require;
    }

    public function hasRequirements() {
        return isSet($this->require) && !is_null($this->require);
    }

    /**
     * Return the successor
     * 
     * @return successor
     */
    public function getSuccessor()
    {
        return $this->successor;
    }
    /**
     * Add the successor
     * 
     * @param String $successor
     */
    public function addSuccessor(ResearchBean $successor)
    {
        $this->successor[] = $successor;
    }

    public function hasSuccessor()
    {
        return isSet($this->successor) && !is_null($this->successor);
    }
    
    /**
     * Return the ongoing
     * 
     * @return ongoing
     */
    public function getOngoing()
    {
        return $this->ongoing;
    }
    /**
     * Set the ongoing
     * 
     * @param int $ongoing
     */
    public function setOngoing($ongoing)
    {
        $this->ongoing = $ongoing;
    }
}
