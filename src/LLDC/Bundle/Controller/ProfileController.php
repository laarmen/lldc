<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use LLDC\Bundle\Form\ProfileType;

use LLDC\Bundle\Util\Util;

/**
 * Controller for the Profile page
 */
class ProfileController extends Controller
{
    /**
     * Displays the profile page
     */
    public function indexAction(Request $request)
    {
        // Form creation
        $form = $this->createForm(ProfileType::class, $this->getUser(), array('action' => $this->generateUrl('profile')))
            ->add($this->get('translator')->trans('realm.profile.update.submit'), SubmitType::class);

        // Form submission
        if($request->getMethod()==='POST') {
            try {
                $form->handleRequest($request);

                if($form->isValid()) {

                    $this->get('session')->set('_locale', $this->getUser()->getLocale());
                    $this->get('translator')->setLocale($this->getUser()->getLocale());

                    $menu = array();
                    foreach(Util::getMenuKeys() as $m) {
                        $menu[$m]['label'] = $this->get('translator')->trans('menu.'.$this->getRealm()->getCharacter()->getRace()->getLabel().'.'.$m.'.label', array(), 'menus');
                        $menu[$m]['description'] = $this->get('translator')->trans('menu.'.$this->getRealm()->getCharacter()->getRace()->getLabel().'.'.$m.'.description', array(), 'menus');
                    }
                    $this->container->get('session')->set('menu', $menu);

                    $this->getManager()->flush();
                    $this->getFlashBag()->add('success', $this->get('translator')->trans('realm.profile.update.success'));
                    return $this->redirect($this->generateUrl('profile'));
                }
            }
            catch(\Exception $e) {
                $this->getFlashBag()->add('error', $this->get('translator')->trans('realm.profile.update.error').' '.$e->getMessage());
            }
        }

        // Else, if form hasn't be submited, or if the submission failed, we display the form
        $data['form'] = $form->createView();
    
        return $this->render('LLDCBundle:Profile:profile.html.twig', $data);
    }
}
