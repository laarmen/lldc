<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Controller\Image;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use LLDC\Bundle\Controller\Controller;
/**
 */
class ImageController extends Controller
{
    public function outputAction(Request $request, $id) {
        $image['metadata'] = $this->getRepository('LLDCBundle:Image')->getMetadata($id);
        $response = new Response();

        /**
         * Type and content
         */
        if($image['metadata']['type']==='jpeg') {
            $response->headers->set('Content-type', 'image/jpeg');
        }

        /**
         * Cache
         */
        $response->setPublic();
        $response->setETag($image['metadata']['checksum']);
        if(!$response->isNotModified($request)) {
            $image = $this->getRepository('LLDCBundle:Image')->getData($id);
            $response->setContent($image['data']);
        }

        return $response;
    }
}
