<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace LLDC\Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as SymfonyController;

/**
 * This abstract class, extended by all Controllers, provides useful shortcuts
 */
abstract class Controller extends SymfonyController {

    /**
     * Returns the current realm
     * @return \LLDC\Bundle\Entity\Realm The current realm
     */
    public function getRealm() {
        return $this->get('doctrine')->getRepository('LLDCBundle:Realm')
            ->findOneBy(array('game' => $this->getGame(), 'user' => $this->getUser()));
    }

    /**
     * Returns the current game
     * @return \LLDC\Bundle\Entity\Game The current game
     */
    public function getGame() {
        return $this->get('session')->get('game');
    }

    /**
     * Returns the doctrine instance
     * @return \Symfony\Bundle\DoctrineBundle\Registry The Doctrine Registry service
     */
    public function getDoctrine() {
        return $this->get('doctrine');
    }

    /**
     * Gets the repository for a class.
     * @param string $className
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository($className) {
        return $this->getDoctrine()->getRepository($className);
    }

    /**
     * Returns the entity manager
     * @return \Symfony\Bundle\DoctrineBundle\Registry The Doctrine Registry service
     */
    public function getManager() {
        return $this->getDoctrine()->getManager();
    }

    /**
     * Returns the flash bag
     * @return \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface The FlashBag interface
     */
    public function getFlashBag() {
        return $this->get('session')->getFlashBag();
    }

    /**
     * Returns the LLDC's configuration
     * @return array(mixed) The LLDC's configuration
     */
    protected function getLLDC() {
        return $this->container->getParameter('lldc');
    }
}
