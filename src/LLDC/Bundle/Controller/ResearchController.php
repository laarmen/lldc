<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Controller
 */
namespace LLDC\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use LLDC\Bundle\Util\Util;

/**
 * Controller for the Researches page
 */
class ResearchController extends Controller
{
    /**
     * Default action
     *
     * @return string The template as a string
     */
    public function researchAction()
    {
        $service = $this->get('lldc.researches');

        $data = array();
        $data['researches'] = $service->getAvailableResearches($this->getRealm());
        ksort($data['researches'], SORT_STRING);
        $data['resources'] = $this->getRealm()->getResource();
        $data['hasOngoingResearch'] = $service->hasOngoingResearch($this->getRealm());
        if($data['hasOngoingResearch']) {
            $data['ongoingResearch'] = $service->getOngoingResearch($this->getRealm());
        }

        return $this->render('LLDCBundle:Research:todo.html.twig', $data);
    }

    /**
     * Launch the developement and redirect the user to the research home page
     *
     * @param Request request
     * @return Redirect the user to the "research" url
     */
    public function researchDevelopAction(Request $request)
    {
        try {
            $service = $this->get('lldc.researches');

            $service->launchResearch($request->get('research'), $this->getRealm());

            $this->getFlashBag()->add('success', $this->get('translator')->trans('realm.researches.todo.launched.success'));
        }
        catch(\Exception $e) {
            $this->getFlashBag()->add('error', $this->get('translator')->trans('realm.researches.todo.launched.error').' '.$e->getMessage());
        }

        return $this->redirect($this->generateUrl('research'));
    }

}
