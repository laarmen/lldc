<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use LLDC\Bundle\Entity\RealmBuilding;
use LLDC\Bundle\Entity\Troop;
use LLDC\Bundle\Util\Util;

/**
 * Controller for the Academy page
 */
class AcademyController extends Controller
{
    /**
     * Displays the units page
     */
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('academyUnit'));
    }

    /**
     *----------------------------------------------------------
     *                      Units
     *----------------------------------------------------------
     */

    /**
     * Displays the units page
     */
    public function unitAction()
    {
        $form = $this->getUnitsForm();

        return $this->render('LLDCBundle:Academy:academy.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function unitBuyAction(Request $request)
    {
        try {
            $service = $this->get('lldc.troop');

            $form = $this->getUnitsForm();
            $form->handleRequest($request);

            foreach($form->getData() as $type => $value) {
	            if(!is_null($value)) {
                    $troop = new Troop();
                    $troop->setType($type);
                    $troop->setAmount($value);
                    $service->buyUnits($troop);
	            }
            }

            $this->getFlashBag()->add('success', $this->get('translator')->trans('realm.academy.units.bought.success'));
        }
        catch(\Exception $e) {
            $this->getFlashBag()->add('error', $this->get('translator')->trans('realm.academy.units.bought.error').' '.$e->getMessage());
        }

        return $this->redirect($this->generateUrl('academyUnit'));
    }

    /**
     * Returns the form for buying units
     */
    private function getUnitsForm()
    {
        $lldc = $this->getLLDC();
        $realm = $this->getRepository('LLDCBundle:Realm')->findOneByUser($this->getUser()->getId());

        $conf = $lldc['races'][$realm->getCharacter()->getRace()->getLabel()];

        $formBuilder = $this->createFormBuilder()
            ->setAction($this->generateUrl('academyUnitBuy'));

        foreach($lldc['units'][$realm->getCharacter()->getRace()->getLabel()] as $key => $value) {
            $formBuilder->add($key, null,
                array(
                    'label' => $this->get('translator')->trans('units.'.$realm->getCharacter()->getRace()->getLabel().'.'.$key.'.plural', array(), 'units'),
                    'required' => false
                )
            );
        }

        $formBuilder->add($this->get('translator')->trans('realm.academy.units.submit'), SubmitType::class);

        return $formBuilder->getForm();
    }

    /**
     *----------------------------------------------------------
     *                      Buildings
     *----------------------------------------------------------
     */
    public function buildingAction()
    {
        $form = $this->getBuildingsForm();

        return $this->render('LLDCBundle:Academy:building.html.twig', array(
            'form' => $form->createView()
        ));
    }
    public function buildingBuyAction(Request $request)
    {
        try {
            $service = $this->get('lldc.buildings');

            $form = $this->getBuildingsForm();
            $form->handleRequest($request);

            foreach($form->getData() as $type => $value) {
	            if(!is_null($value)) {
                    $realmBuilding = new RealmBuilding();
                    $realmBuilding->setType($type);
                    $realmBuilding->setAmount($value);
                    $service->buyBuildings($realmBuilding);
	            }
            }

            $this->getFlashBag()->add('success', $this->get('translator')->trans('realm.academy.buildings.bought.success'));
        }
        catch(\Exception $e) {
            $this->getFlashBag()->add('error', $this->get('translator')->trans('realm.academy.buildings.bought.error').' '.$e->getMessage());
        }

        return $this->redirect($this->generateUrl('academyBuilding'));
    }

    /**
     * Returns the form for buying buildings
     */
    private function getBuildingsForm()
    {
        $lldc = $this->container->getParameter('lldc');
        $realm = $this->getRealm();

        $realmBuilding = new RealmBuilding();

        $formBuilder = $this->createFormBuilder()
            ->setAction($this->generateUrl('academyBuildingBuy'));

        foreach($lldc['buildings'][$realm->getCharacter()->getRace()->getLabel()] as $key => $value) {
            $formBuilder->add($key, null,
                array(
                    'label' => $this->get('translator')->trans('buildings.'.$realm->getCharacter()->getRace()->getLabel().'.'.$key.'.plural', array(), 'buildings'),
                    'required' => false
                )
            );
        }

        $formBuilder->add($this->get('translator')->trans('realm.academy.buildings.submit'), SubmitType::class);

        return $formBuilder->getForm();
    }
}
