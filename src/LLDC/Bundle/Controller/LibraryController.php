<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Controller
 */
namespace LLDC\Bundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

use LLDC\Bundle\Util\Util;
use LLDC\Bundle\Form\RpCharacterType;

/**
 * Controller for the Library page
 */
class LibraryController extends Controller
{
    /**
     * Default action
     *
     * @return string The template as a string
     */
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('libraryCharacter'));
    }

    /**
     * This method displays the list of realm characters.
     *
     * @return string The template as a string
     */
    public function characterAction()
    {
        $realm = $this->getRepository('LLDCBundle:Realm')->findOneBy(array('game' => $this->getGame(), 'user' => $this->getUser()));

        // List of all the characters of the user
        $data['characters'] = $this->getUser()->getCharacters();

        return $this->render('LLDCBundle:Library:character.html.twig', $data);
    }

    /**
     * This method displays the form for editing a character, and handle the submission.
     *
     * @param int $id RpCharacter's id (passed by GET)
     * @return string The template as a string
     */
    public function characterEditAction(Request $request, $id)
    {
        $character = $this->getRepository('LLDCBundle:RpCharacter')->findOneById($id);

        // If the character is owned by the current user
        if($character != null && $character->getRealm()->getUser() == $this->getUser()) {

            // Form creation
            $form = $this->createForm(RpCharacterType::class, $character, array('action' => $this->generateUrl('libraryCharacterEdit', array('id' => $id))))
                ->add($this->get('translator')->trans('realm.character.form.edit.submit'), SubmitType::class);

            if(!empty($character->getAvatar())) {
                // Indicates to the template that there is already a picture defined, so it can be displayed
                $data['avatar'] = true;
            }
            // Form submission
            if($request->getMethod()==='POST') {
                try {
                    $form->handleRequest($request);

                    if($form->isValid()) {
                        $character->handleUpload(
                            $this->getManager(),
                            $request->files->get('lldc_bundle_rpcharacter')['avatar']);
                        // Persist the modifications
                        $this->getManager()->flush();

                        $this->getFlashBag()->add('success', $this->get('translator')->trans('realm.character.form.edit.success'));
                        return $this->redirect($this->generateUrl('library'));
                    }
                }
                catch(\Exception $e) {
                    $this->getFlashBag()->add('error', $this->get('translator')->trans('realm.character.form.edit.error').' '.$e->getMessage());
                }
            }

            // Else, if form hasn't be submited, or if the submission failed, we display the form
            $data['character'] = $character;
            $data['form'] = $form->createView();

            return $this->render('LLDCBundle:Library:characterEdit.html.twig', $data);
        }
        // Else, the access is denied
        else {
            $this->getFlashBag()->add('error', $this->get('translator')->trans('access.denied'));
            return $this->redirect($this->generateUrl('library'));
        }
    }
}
