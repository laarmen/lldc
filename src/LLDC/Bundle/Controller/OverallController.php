<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Controller;

/**
 * Controller for all internal blocks, like panel, tabs or searchUser tool
 */
class OverallController extends Controller
{
	/**
	 * Renders the panel.html.twig template, which fills the panel block
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function panelAction()
    {
        $panelData = array();

        $realm = $this->getRepository('LLDCBundle:Realm')->findOneByUser($this->getUser()->getId());
        
        $panelData['food'] = $realm->getResource()->getFood();
        $panelData['wood'] = $realm->getResource()->getWood();
        $panelData['rock'] = $realm->getResource()->getRock();
        $panelData['gold'] = $realm->getResource()->getGold();
        $panelData['madrens'] = $realm->getResource()->getMadrens();
        $panelData['morale'] = $this->get('lldc.resources')->getMoraleHumanized($realm->getResource()->getMorale());
        $panelData['moraleValue'] = $realm->getResource()->getMorale();
        $panelData['xp'] = $realm->getResource()->getXp();
        foreach($realm->getRanking() as $ranking) {
            $panelData[$ranking->getType()] = array('value' => $ranking->getValue(), 'position' => $ranking->getPosition());
        }

        return $this->render('LLDCBundle:Overall:panel.html.twig', $panelData);
    }

    /**
     * Renders the tabs.html.twig template, which fills the tabs_block
     * 
     * @param string $callFrom
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tabsAction($callFrom)
    {
        return $this->render('LLDCBundle:Overall:tabs.html.twig', array('tabs' => $this->get('lldc.tabs')->getTabs($callFrom)));
    }

    public function searchUserAction($call = 'UserAction', $withGame = false)
    {
        return $this->render('LLDCBundle:Overall:searchUser.html.twig',
            array(
                'call' => $call,
                'withGame' => $withGame
            ));
    }
}
