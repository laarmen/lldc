<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Controller;

use LLDC\Bundle\Util\Util;

/**
 * Controller for the Realm page
 */
class RealmController extends Controller
{
    public function indexAction()
    {
        $doctrine = $this->getDoctrine();
        $realm = $this->getRealm();

        /**
         * Retrieving data
         */
        $data = array();

        $units = array();
        foreach($realm->getTroop() as $troop)
        {
            $units[$troop->getType()][] = array(
                'key' => $troop->getType(),
                'amount' => $troop->getAmount()
            );
        }
        $data['units'] = $units;

        $buildings = array();
        foreach($realm->getBuilding() as $building)
        {
            $buildings[$building->getType()] = array(
                'key' => $building->getType(),
                'amount' => $building->getAmount()
            );
        }
        $data['buildings'] = $buildings;

        if(!$realm->getOngoingBuilding()->isEmpty()) {
            $ongoingBuildings = array();
            foreach($this->getLLDC()['buildings'][$realm->getCharacter()->getRace()->getLabel()] as $key => $value) {
                foreach($realm->getOngoingBuilding() as $ongoingBuilding) {
                    if($ongoingBuilding->getType()===$key) {
                        if($ongoingBuilding->getDateBegin()==$ongoingBuilding->getDateLastWork()) {
                            $ongoingBuildings[$key]['waiting'][] = $ongoingBuilding;
                        }
                        else {
                            $ongoingBuildings[$key]['current'][] = $ongoingBuilding;
                        }
                    }
                }
            }
            $data['ongoingBuildings'] = $ongoingBuildings;
        }

        $data['realm'] = $realm;

        return $this->render('LLDCBundle:Realm:realm.html.twig', $data);
    }

    public function manageBattlefieldAction() {
        $battlefields = $this->getRealm()->getPlace()->getBattlefields();
        return $this->render('LLDCBundle:Realm:manage_battlefield.html.twig', array('battlefields' => $battlefields));
    }
}
