<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Controller\Webservice;

use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for the webservice Notifications
 */
class NotificationController extends WebserviceController
{
    /**
     * Displays notifications for the current realm, and destroys them.
     * Headers and flush() method are part of the HTML5 Push feature.
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function notificationAction()
    {
        // We retrieve all the notifications for this realm
        $notifications = $this->getRealm()->getNotification();
        $array = array();
        $data = '';
        if($notifications != null) {
            // We add each notification in the global array, and remove them from the database
            foreach($notifications as $notification) {
                $array[] = array('content' => $notification->getContent(), 'type' => $notification->getType(), 'path' => $notification->getPath());
                $this->container->get('doctrine')->getEntityManager()->remove($notification);
            }
            // If they are messages, we encode them in JSON
            if(count($array) > 0) {
                $data = json_encode($array, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT);
            }
        }

        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
        echo "data: ".$data."\n\n";

        // We flush the db modifications
        $this->container->get('doctrine')->getEntityManager()->flush();
        // We flush the content (only used for HTML5 push)
        flush();

        // Symfony wants a Response object to be returned, so...
        return new Response();
    }
}
