<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace LLDC\Bundle\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Console\Application;

use LLDC\Bundle\Command\Admin\UserDeleteCommand;

/**
 * Test class for the lldc:admin:user:delete command
 */
class UserDeleteCommandTest extends WebTestCase
{
    /**
     * Set up the test
     */
    public function setUp()
    {
        self::bootKernel();
    }
    
    /**
     * Check if the command displays something... that seems to be good
     */
    public function testUserDeleteCommandTest()
    {
        $command = new UserDeleteCommand();
        $application = new Application(static::$kernel);
        $command->setApplication($application);
        $commandTester = new CommandTester($command);
        $commandTester->execute(array('command' => $command->getName(), '--user-id' => '0'));
        $this->assertRegExp('/The command ended properly/', $commandTester->getDisplay());
    }
}
