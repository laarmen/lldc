<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace LLDC\Bundle\EventListener;

use LLDC\Bundle\Controller\Controller;
use LLDC\Bundle\Controller\Webservice\NotificationController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Listening on kernel events
 */
class KernelListener implements EventSubscriberInterface
{
	private $container;
	public function __construct($container) {
		$this->container = $container;
	}

    public static function getSubscribedEvents()
    {
        return array();
    }

    /**
     * @see http://symfony.com/fr/doc/current/cookbook/event_dispatcher/before_after_filters.html
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if(!is_array($controller)) {
            return;
        }
        $controller = $controller[0];
        /**
         * To dev :
         *  - Don't forget to blacklist every Controller which shouldn't trig an update of the last action date
         * - e.g : NotificationController
         */
        if($controller instanceof Controller) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            if(!is_null($user)) {
                if(!$controller instanceof NotificationController) {
                    // Every 60 seconds, we update the date_last_action field in the user table, for each NOT AJAX Controller called
                    $now = new \Datetime();
                    if($user->getDateLastAction() <= $now->sub(new \Dateinterval("PT60S"))) {
                        $user->setDateLastAction(new \Datetime());
                        $controller->getManager()->flush();
                    }
                }
            }
        }
    }
}
