<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RealmProduction
 */
class RealmProduction
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $foodProdPercent;

    /**
     * @var integer
     */
    private $woodProdPercent;

    /**
     * @var integer
     */
    private $rockProdPercent;

    /**
     * @var integer
     */
    private $goldProdPercent;

    /**
     * @var integer
     */
    private $buildingProdPercent;

    /**
     * @var \LLDC\Bundle\Entity\Realm
     */
    private $realm;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set foodProdPercent
     *
     * @param integer $foodProdPercent
     * @return RealmProduction
     */
    public function setFoodProdPercent($foodProdPercent)
    {
        $this->foodProdPercent = $foodProdPercent;

        return $this;
    }

    /**
     * Get foodProdPercent
     *
     * @return integer
     */
    public function getFoodProdPercent()
    {
        return $this->foodProdPercent;
    }

    /**
     * Set woodProdPercent
     *
     * @param integer $woodProdPercent
     * @return RealmProduction
     */
    public function setWoodProdPercent($woodProdPercent)
    {
        $this->woodProdPercent = $woodProdPercent;

        return $this;
    }

    /**
     * Get woodProdPercent
     *
     * @return integer
     */
    public function getWoodProdPercent()
    {
        return $this->woodProdPercent;
    }

    /**
     * Set rockProdPercent
     *
     * @param integer $rockProdPercent
     * @return RealmProduction
     */
    public function setRockProdPercent($rockProdPercent)
    {
        $this->rockProdPercent = $rockProdPercent;

        return $this;
    }

    /**
     * Get rockProdPercent
     *
     * @return integer
     */
    public function getRockProdPercent()
    {
        return $this->rockProdPercent;
    }

    /**
     * Set goldProdPercent
     *
     * @param integer $goldProdPercent
     * @return RealmProduction
     */
    public function setGoldProdPercent($goldProdPercent)
    {
        $this->goldProdPercent = $goldProdPercent;

        return $this;
    }

    /**
     * Get goldProdPercent
     *
     * @return integer
     */
    public function getGoldProdPercent()
    {
        return $this->goldProdPercent;
    }

    /**
     * Set buildingProdPercent
     *
     * @param integer $buildingProdPercent
     * @return RealmProduction
     */
    public function setBuildingProdPercent($buildingProdPercent)
    {
        $this->buildingProdPercent = $buildingProdPercent;

        return $this;
    }

    /**
     * Get buildingProdPercent
     *
     * @return integer
     */
    public function getBuildingProdPercent()
    {
        return $this->buildingProdPercent;
    }

    /**
     * Set realm
     *
     * @param \LLDC\Bundle\Entity\Realm $realm
     * @return RealmProduction
     */
    public function setRealm(\LLDC\Bundle\Entity\Realm $realm = null)
    {
        $this->realm = $realm;

        return $this;
    }

    /**
     * Get realm
     *
     * @return \LLDC\Bundle\Entity\Realm
     */
    public function getRealm()
    {
        return $this->realm;
    }
}
