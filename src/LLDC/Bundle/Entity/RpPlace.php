<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RpPlace
 */
class RpPlace
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $picture;

    /**
     * @var \LLDC\Bundle\Entity\Battlefield
     */
    private $battlefields;

    /**
     * @var \LLDC\Bundle\Entity\Realm
     */
    private $realm;

    /**
     * @var \LLDC\Bundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RpPlace
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set picture
     *
     * @param string $picture
     * @return RpPlace
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return string 
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set battlefield
     *
     * @param \LLDC\Bundle\Entity\Battlefield $battlefield
     * @return RpPlace
     */
    public function setBattlefield(\LLDC\Bundle\Entity\Battlefield $battlefield = null)
    {
        $this->battlefield = $battlefield;

        return $this;
    }

    /**
     * Get battlefield
     *
     * @return \LLDC\Bundle\Entity\Battlefield 
     */
    public function getBattlefields()
    {
        return $this->battlefields;
    }

    /**
     * Set realm
     *
     * @param \LLDC\Bundle\Entity\Realm $realm
     * @return RpPlace
     */
    public function setRealm(\LLDC\Bundle\Entity\Realm $realm = null)
    {
        $this->realm = $realm;

        return $this;
    }

    /**
     * Get realm
     *
     * @return \LLDC\Bundle\Entity\Realm 
     */
    public function getRealm()
    {
        return $this->realm;
    }

    /**
     * Set user
     *
     * @param \LLDC\Bundle\Entity\User $user
     * @return RpPlace
     */
    public function setUser(\LLDC\Bundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \LLDC\Bundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->battlefield = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add battlefield
     *
     * @param \LLDC\Bundle\Entity\Battlefield $battlefield
     * @return RpPlace
     */
    public function addBattlefield(\LLDC\Bundle\Entity\Battlefield $battlefield)
    {
        $this->battlefield[] = $battlefield;

        return $this;
    }

    /**
     * Remove battlefield
     *
     * @param \LLDC\Bundle\Entity\Battlefield $battlefield
     */
    public function removeBattlefield(\LLDC\Bundle\Entity\Battlefield $battlefield)
    {
        $this->battlefield->removeElement($battlefield);
    }
}
