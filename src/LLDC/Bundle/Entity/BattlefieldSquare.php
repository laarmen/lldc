<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BattlefieldSquare
 *
 * All the computations are done using the following blog post as a guide:
 *
 *   http://www.redblobgames.com/grids/hexagons/
 *
 */
class BattlefieldSquare implements \JsonSerializable {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $q;

    /**
     * @var integer
     */
    private $r;

    /**
     * @var integer
     */
    private $x;

    /**
     * @var integer
     */
    private $y;

    /**
     * @var integer
     */
    private $z;

    /**
     * @var string
     */
    private $groundType;

    /**
     * @var \LLDC\Bundle\Entity\Battlefield
     */
    private $battlefield;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get x
     *
     * @return integer
     */
    public function getX() {
        return $this->x;
    }

    /**
     * Get y
     *
     * @return integer
     */
    public function getY() {
        return $this->y;
    }

    /**
     * Get z
     *
     * @return integer
     */
    public function getZ() {
        return $this->z;
    }

    /**
     * Set q (AKA x)
     *
     * @param type $q
     */
    public function setQ($q) {
        $this->q = $q;
        $this->computeCubicCoordinates();
    }

    /**
     * Set r (AKA z)
     *
     * @param type $r
     */
    public function setR($r) {
        $this->r = $r;
        $this->computeCubicCoordinates();
    }

    /**
     * Set battlefield
     *
     * @param \LLDC\Bundle\Entity\Battlefield $battlefield
     * @return BattlefieldSquare
     */
    public function setBattlefield(\LLDC\Bundle\Entity\Battlefield $battlefield = null) {
        $this->battlefield = $battlefield;

        return $this;
    }

    /**
     * Get battlefield
     *
     * @return \LLDC\Bundle\Entity\War
     */
    public function getBattlefield() {
        return $this->battlefield;
    }

    /**
     * @var \LLDC\Bundle\Entity\Troop
     */
    private $troop;

    /**
     * Set troop
     *
     * @param \LLDC\Bundle\Entity\Troop $troop
     * @return BattlefieldSquare
     */
    public function setTroop(\LLDC\Bundle\Entity\Troop $troop = null) {
        $this->troop = $troop;

        return $this;
    }

    /**
     * Get troop
     *
     * @return \LLDC\Bundle\Entity\Troop
     */
    public function getTroop() {
        return $this->troop;
    }

    /**
     * Set groundType
     *
     * @param string $groundType
     * @return BattlefieldSquare
     */
    public function setGroundType($groundType) {
        $this->groundType = $groundType;

        return $this;
    }

    /**
     * Get groundType
     *
     * @return string
     */
    public function getGroundType() {
        return $this->groundType;
    }

    /**
     * Get q
     *
     * This function is private as the axial coordinates are only used for
     * storage
     *
     * @return integer
     */
    private function getQ() {
        return $this->q;
    }

    /** Get r
     *
     * This function is private as the axial coordinates are only used for
     * storage
     *
     * @return integer
     */
    private function getR() {
        return $this->r;
    }

    /**
     * Get q as an "odd-r" offset.
     *
     * @return integer
     */
    public function getOffsetQ() {
        return $this->x + ($this->z - ($this->z & 1)) / 2;
    }

    /**
     * Get r as an "odd-r" offset.
     *
     * @return integer
     */
    public function getOffsetR() {
        return $this->z;
    }

    /**
     * @ORM\PostLoad
     */
    public function computeCubicCoordinates() {
        $this->x = $this->getQ();
        $this->z = $this->getR();
        $this->y = -($this->x) - ($this->z);
    }

    public function jsonSerialize() {
        $this->computeCubicCoordinates();
        $fields = [
            "id" => $this->id,
            "groundType" => $this->groundType,
            "x" => $this->x,
            "y" => $this->y,
            "z" => $this->z
        ];
        if ($this->getTroop() != null) {
            $fields['troop'] = $this->getTroop()->getId();
        }
        return $fields;
    }

}
