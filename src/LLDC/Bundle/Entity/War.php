<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * War
 */
class War
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateCreation;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $realm;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->realm = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return War
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    
        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Add realm
     *
     * @param \LLDC\Bundle\Entity\Realm $realm
     * @return War
     */
    public function addRealm(\LLDC\Bundle\Entity\Realm $realm)
    {
        $this->realm[] = $realm;
    
        return $this;
    }

    /**
     * Remove realm
     *
     * @param \LLDC\Bundle\Entity\Realm $realm
     */
    public function removeRealm(\LLDC\Bundle\Entity\Realm $realm)
    {
        $this->realm->removeElement($realm);
    }

    /**
     * Get realm
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRealm()
    {
        return $this->realm;
    }
    /**
     * @var \LLDC\Bundle\Entity\Battlefield
     */
    private $battlefield;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $attackers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $defenders;


    /**
     * Set battlefield
     *
     * @param \LLDC\Bundle\Entity\Battlefield $battlefield
     * @return War
     */
    public function setBattlefield(\LLDC\Bundle\Entity\Battlefield $battlefield = null)
    {
        $this->battlefield = $battlefield;
    
        return $this;
    }

    /**
     * Get battlefield
     *
     * @return \LLDC\Bundle\Entity\WarBattlefield 
     */
    public function getBattlefield()
    {
        return $this->battlefield;
    }

    /**
     * Add attackers
     *
     * @param \LLDC\Bundle\Entity\Realm $attackers
     * @return War
     */
    public function addAttacker(\LLDC\Bundle\Entity\Realm $attackers)
    {
        $this->attackers[] = $attackers;
    
        return $this;
    }

    /**
     * Remove attackers
     *
     * @param \LLDC\Bundle\Entity\Realm $attackers
     */
    public function removeAttacker(\LLDC\Bundle\Entity\Realm $attackers)
    {
        $this->attackers->removeElement($attackers);
    }

    /**
     * Get attackers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttackers()
    {
        return $this->attackers;
    }

    /**
     * Add defenders
     *
     * @param \LLDC\Bundle\Entity\Realm $defenders
     * @return War
     */
    public function addDefender(\LLDC\Bundle\Entity\Realm $defenders)
    {
        $this->defenders[] = $defenders;
    
        return $this;
    }

    /**
     * Remove defenders
     *
     * @param \LLDC\Bundle\Entity\Realm $defenders
     */
    public function removeDefender(\LLDC\Bundle\Entity\Realm $defenders)
    {
        $this->defenders->removeElement($defenders);
    }

    /**
     * Get defenders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDefenders()
    {
        return $this->defenders;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $troop;


    /**
     * Add troop
     *
     * @param \LLDC\Bundle\Entity\Troop $troop
     * @return War
     */
    public function addTroop(\LLDC\Bundle\Entity\Troop $troop)
    {
        $this->troop[] = $troop;
    
        return $this;
    }

    /**
     * Remove troop
     *
     * @param \LLDC\Bundle\Entity\Troop $troop
     */
    public function removeTroop(\LLDC\Bundle\Entity\Troop $troop)
    {
        $this->troop->removeElement($troop);
    }

    /**
     * Get troop
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTroop()
    {
        return $this->troop;
    }
}
