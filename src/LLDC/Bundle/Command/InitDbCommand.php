<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command
 */
namespace LLDC\Bundle\Command;

use LLDC\Bundle\Command\Generate\GenerateConfigCommand;
use LLDC\Bundle\LLDCException;
use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use LLDC\Bundle\Entity\Game;
use LLDC\Bundle\Entity\GameInitialTroop;
use LLDC\Bundle\Entity\GameInitialBuilding;
use LLDC\Bundle\Entity\Gender;
use LLDC\Bundle\Entity\Race;

/**
 * This command initializes the database. It inserts the main game, based on the lldc.yml, then, inserts the races and the genders.
 * * Usage : <b>php app/console lldc:init-db</b>
 */
class InitDbCommand extends LLDCCommand
{
    /**
     * @inherited
     */
    protected function configure()
    {
        $this
            ->setName('lldc:init-db')
            ->setDescription('Initializes the database')
            ->setHelp('This command initializes the database. It inserts the main game, based on the lldc.yml, then, inserts the races and the genders.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getManager();

        $output->writeln("<comment>Database initialization...</comment>");

        // Fetching the main game to initialize realm settings
        $game = $this->getRepository('LLDCBundle:Game')->findOneByMain(1);
        if($game == null) {

            // Fetching parameters
            $lldc = $this->getLLDC();

            $main = $lldc['game']['main'];

            $raceElves = new Race();
            $raceElves->setLabel('elves');

            $raceHumans = new Race();
            $raceHumans->setLabel('humans');

            $raceDwarves = new Race();
            $raceDwarves->setLabel('dwarves');

            $raceNecromancers = new Race();
            $raceNecromancers->setLabel('necromancers');

            $raceRhorks = new Race();
            $raceRhorks->setLabel('rhorks');

            $raceKorrigans = new Race();
            $raceKorrigans->setLabel('korrigans');

            $races = array($raceElves, $raceHumans, $raceDwarves, $raceNecromancers, $raceRhorks, $raceKorrigans);

            // Creating the races
            $output->writeln("Adding races");

            $doctrine->persist($raceElves);
            $doctrine->persist($raceHumans);
            $doctrine->persist($raceDwarves);
            $doctrine->persist($raceNecromancers);
            $doctrine->persist($raceRhorks);
            $doctrine->persist($raceKorrigans);

            // Creating the genders
            $genderMale = new Gender();
            $genderMale->setLabel('male');

            $genderFemale = new Gender();
            $genderFemale->setLabel('female');

            $output->writeln("Adding genders");

            $doctrine->persist($genderMale);
            $doctrine->persist($genderFemale);

            // Creating the main Game
            $game = new Game();
            $game->setMain(1);
            $game->setLabel($main['label']);
            $game->setDescription($main['description']);
            $game->setDateStart(new \DateTime($main['date_start']));
            $game->setDateEnd(new \DateTime($main['date_end']));

            $game->setInitialResourcesFood($main['initial']['resources']['food']);
            $game->setInitialResourcesWood($main['initial']['resources']['wood']);
            $game->setInitialResourcesRock($main['initial']['resources']['rock']);
            $game->setInitialResourcesGold($main['initial']['resources']['gold']);
            $game->setInitialResourcesMadrens($main['initial']['resources']['madrens']);
            $game->setInitialResourcesMorale($main['initial']['resources']['morale']);
            $game->setInitialResourcesXp($main['initial']['resources']['xp']);
            $game->setInitialResourcesAcres($main['initial']['resources']['acres']);

            $game->setInitialProductionFood($main['initial']['production']['food']);
            $game->setInitialProductionWood($main['initial']['production']['wood']);
            $game->setInitialProductionRock($main['initial']['production']['rock']);
            $game->setInitialProductionGold($main['initial']['production']['gold']);
            $game->setInitialProductionBuilding($main['initial']['production']['building']);

            $game->setBuildingsCost($main['buildings']['cost']);
            $game->setBuildingsQueueLength($main['buildings']['queue_length']);
            $game->setBuildingsSpeed($main['buildings']['speed']);

            $game->setProductionRateFood($main['production_rate']['food']);
            $game->setProductionRateWood($main['production_rate']['wood']);
            $game->setProductionRateRock($main['production_rate']['rock']);
            $game->setProductionRateGold($main['production_rate']['gold']);
            $game->setProductionRateMadrens($main['production_rate']['madrens']);

            $game->setResearchesCost($main['researches']['cost']);
            $game->setResearchesSpeed($main['researches']['speed']);

            $game->setUnitsCapacity($main['units']['capacity']);
            $game->setUnitsCost($main['units']['cost']);
            $game->setUnitsMaintenanceFactor($main['units']['maintenance']['factor']);
            $game->setUnitsMaintenanceDuringInternalWar($main['units']['maintenance']['during_internal_war']);
            $game->setUnitsMaintenanceDuringExternalWar($main['units']['maintenance']['during_external_war']);
            $game->setUnitsMaintenanceMoraleLoss($main['units']['maintenance']['morale_loss']);
            $game->setUnitsPoints($main['units']['points']);
            $game->setUnitsSpeed($main['units']['speed']);

            $game->setBattlefieldsSize($main['battlefields']['size']);
            $game->setBattlefieldsMaxAmount($main['battlefields']['max_amount']);

            $output->writeln("Adding main game");

            $doctrine->persist($game);

            // Creating the initial units
            $output->writeln("Adding initial troops");
            $units = $main['initial']['units'];
            foreach($races as $race) {
                /** @var LLDC\Bundle\Entity\Race $race */

                // It is allowed to specify the units for a specific race.
                if (isSet($units[$race->getLabel()])) {
                    $race_units = $units[$race->getLabel()];
                } else {
                    $race_units = $units['default'];
                }

                if(is_null($race_units)) {
                    $generateConfigCommand = new GenerateConfigCommand();
                    throw new LLDCException('There is a problem in your lldc.yml file. Did you generate it ? php app/console '.$generateConfigCommand->getName());
                }

                foreach($race_units as $label => $amount) {
                    // Check the availability of the troop for the race.
                    if (!isSet($lldc['units'][$race->getLabel()][$label])) {
                        $output->writeln('<error>The unit "'.$label.'" isn\'t available for the race "'.$race->getLabel().'".</error>');
                        exit;
                    }
                    $unit = new GameInitialTroop();
                    $unit->setType($label);
                    $unit->setAmount($amount);
                    $unit->setGame($game);
                    $unit->setRace($race);

                    $doctrine->persist($unit);
                }
            }

            // Creating the initial buildings
            $output->writeln("Adding initial buildings");
            $buildings = $main['initial']['buildings'];
            foreach($races as $race) {
                /** @var LLDC\Bundle\Entity\Race $race */

                // It is allowed to specify the buildings for a specific race.
                if (isSet($buildings[$race->getLabel()])) {
                    $race_buildings = $buildings[$race->getLabel()];
                } else {
                    $race_buildings = $buildings['default'];
                }

                foreach($race_buildings as $label => $amount) {
                    // Check if the building is actually available for this race.
                    if (!isSet($lldc['buildings'][$race->getLabel()][$label])) {
                        $output->writeln('<error>The building "'.$label.'" isn\'t available for the race "'.$race->getLabel().'".</error>');
                        exit;
                    }
                    $building = new GameInitialBuilding();
                    $building->setType($label);
                    $building->setAmount($amount);
                    $building->setGame($game);
                    $building->setRace($race);

                    $doctrine->persist($building);
                }
            }

            $output->writeln("Inserting into the database");
            $doctrine->flush();

            $output->writeln('<info>The database has been initialized.</info>');
        }
        else {
            $output->writeln('<info>The database is already initialized.</info>');
        }

        $this->end($output);
    }
}
