<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command
 */
namespace LLDC\Bundle\Command;

use LLDC\Bundle\Command\Generate\GenerateConfigCommand;
use LLDC\Bundle\LLDCException;
use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use LLDC\Bundle\Entity\Game;
use LLDC\Bundle\Entity\GameInitialTroop;
use LLDC\Bundle\Entity\GameInitialBuilding;
use LLDC\Bundle\Entity\Gender;
use LLDC\Bundle\Entity\Race;

/**
 * This command initializes the database. It inserts the main game, based on the lldc.yml, then, inserts the races and the genders.
 * * Usage : <b>php app/console lldc:init-db</b>
 */
class FakerPopulateDbCommand extends LLDCCommand
{
    /**
     * @inherited
     */
    protected function configure()
    {
        $this
            ->setName('lldc:faker:populate')
            ->setDescription('Initializes the database with fake data')
            ->setHelp('This command initializes the database with fake data for testing purpose.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getManager();

        $output->writeln("<comment>Fake database populate...</comment>");

        // Fetching the main game to initialize realm settings
        $game = $this->getRepository('LLDCBundle:Game')->findOneByMain(1);
        if(is_null($game)) {
			$output->writeln('<error>No game found, execute lldc:init-db first</error>');
		}
		
		/**
		 * Creation of 2 realms
		 */
		 
        // We use the FOSBundle
        $manager = $this->getContainer()->get('fos_user.user_manager');
		
		
		$faker = $this->getContainer()->get('faker.generator');
		$faker->seed(1337);

		for($i = 0; $i < 6; $i++) {
			$user = $manager->createUser();
			$user->setUsername($faker->username);
			$user->setEmail($faker->email);
			$user->setPlainPassword($faker->password);
			$user->setEnabled(true);
			$user->setSuperAdmin(false);
			$user->setConfirmationToken(null);
			$user->setRegistrationRealmName($faker->firstName);
			$user->setRegistrationRace($faker->randomElement(['dwarves', 'elves', 'humans', 'korrigans', 'necromancers', 'rhorks']));
			$user->setRegistrationIdGuild(0);
			$user->setRegistrationGender($faker->randomElement(['male', 'female']));
			$user->setLocale($faker->randomElement(['fr', 'frRP']));
			$user->setDateLastAction(new \Datetime());

			$manager->updateUser($user);

			$service = $this->getContainer()->get('lldc.user');
			$service->createUser($user);
		}

		$doctrine->flush();

        $this->end($output);
    }
}
