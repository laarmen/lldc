<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Generate
 */
namespace LLDC\Bundle\Command\Generate;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This command generates you the content of the crontab for LLDC.
 * * Usage : <b>php app/console lldc:generate:crontab</b>
 * * Result (ex.) : <i>* /1 * * * * php /var/www/lldc/app/console lldc:realm:resource:produce -q</i>
 *
 * You can copy/paste the result of this command into your crontab with <b>crontab -e</b>
 */
class GenerateCrontabCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:generate:crontab')
            ->setDescription('Generates the crontab so that you can install it yourself easily')
            ->setHelp('This command generates you the content of the crontab for LLDC.'."\n\n".
            'Result (ex.) : <comment>*/1 * * * * php /var/www/lldc/app/console lldc:realm:resource:produce -q</comment>'."\n\n".
            'You can copy/paste the result of this command into your crontab with <info>crontab -e</info>'."\n".
            'That\'s all !')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write('<info>Voici les lignes à ajouter dans votre crontab :</info>'."\n");
        $output->write("<comment># LLDC ---------------------------------------------------------------------------------------------------------------</comment>"."\n");

        $output->write('*/1 * * * * php '.$this->getContainer()->get('kernel')->getRootDir().'/console lldc:realm:resource:produce -q');
        $output->write("\n");
        $output->write('*/1 * * * * php '.$this->getContainer()->get('kernel')->getRootDir().'/console lldc:realm:ongoing:buildings -q');
        $output->write("\n");
        $output->write('*/1 * * * * php '.$this->getContainer()->get('kernel')->getRootDir().'/console lldc:realm:ongoing:researches -q');
        $output->write("\n");
        $output->write('* */1 * * * php '.$this->getContainer()->get('kernel')->getRootDir().'/console lldc:realm:troop:maintenance -q');
        $output->write("\n");

        $output->write("<comment># LLDC ---------------------------------------------------------------------------------------------------------------</comment>");

        $this->end($output);
    }
}
