<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command
 */
namespace LLDC\Bundle\Command;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

use LLDC\Bundle\Entity\Game;
use LLDC\Bundle\Entity\Gender;
use LLDC\Bundle\Entity\Race;

/**
 * This command allow you to test the configuration file.
 * For example :
 *  php app/console lldc:test-config abilities training-dagger 
 * Will display the parameters contained in parameters.lldc.abilities.training-dagger
 */
class CheckConfigCommand extends LLDCCommand
{
    /**
     * @inherited
     */
    protected function configure()
    {
        $this
            ->setName('lldc:check-config')
            ->setDescription('Displays the configuration')
            ->setHelp('Displays the configuration. You can navigate through the different levels.')
            ->addArgument('first', InputArgument::OPTIONAL)
            ->addArgument('second', InputArgument::OPTIONAL)
            ->addArgument('third', InputArgument::OPTIONAL)
            ->addArgument('fourth', InputArgument::OPTIONAL)
            ->addArgument('fifth', InputArgument::OPTIONAL)
            ->addArgument('sixth', InputArgument::OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $this->getLLDC();

        $depths = array('first', 'second', 'third', 'fourth', 'fifth', 'sixth');
        $firstLine = '';

        // Reach the parameter
        foreach($depths as $depth) {
            $firstLine .= $input->getArgument($depth).' ';
            if($input->getArgument($depth)) {
                if(isSet($config[$input->getArgument($depth)])) {
                    $config = $config[$input->getArgument($depth)];
                }
                else {
                    $output->writeln('<error>Incorrect parameter : '.$input->getArgument($depth).'</error>');
                    exit;
                }
            }
        }
        $display = var_export($config, true);

        $search     = array('#(.*) =>#',                                '#,#',  '#array \(#',   '#\)#', '#\'(.*)\'<#', "/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/");
        $replace    = array('<info>$1</info> <comment>:</comment>',    '',     '',             '',     '$1<', "\n");
        $display = preg_replace($search, $replace, $display);

        $output->writeln('<info>'.$firstLine.'</info>'.$display);

        $this->end($output);
    }
}
