<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Admin
 */
namespace LLDC\Bundle\Command\Admin;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * This command deletes a user.
 * * Usage : <b>php app/console lldc:admin:user:delete --user-id=<ID></b>
 */
class UserDeleteCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:admin:user:delete')
            ->addOption('user-id', null, InputOption::VALUE_REQUIRED,  "User's ID")
            ->setDescription('Delete a user')
            ->setHelp('This command completly deletes a user from the database.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userId = $input->getOption('user-id');
        $output->writeln("<comment>User n°".$userId." will be deleted...</comment>");

        $user = $this->getRepository('LLDCBundle:User')->findOneById($userId);
        if($user==null) {
            $output->writeln("<error>No user found for the id ".$userId."</error>");
        }
        else {
            $service = $this->getContainer()->get('lldc.user');
            $service->deleteUser($user);
            $output->writeln("<info>User n°".$userId." deleted successfully.</info>");
        }

        $this->end($output);
    }
}
