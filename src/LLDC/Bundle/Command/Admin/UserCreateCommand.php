<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Admin
 */
namespace LLDC\Bundle\Command\Admin;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

use FOS\UserBundle\Util\UserManipulator;
use FOS\UserBundle\Util\Canonicalizer;

use LLDC\Bundle\Entity\User;

/**
 * This command deletes a user.
 * * Usage : <b>php app/console lldc:admin:user:create </b>
 */
class UserCreateCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:admin:user:create')
            ->addOption('username',     'u',    InputOption::VALUE_REQUIRED,  "Username")
            ->addOption('realmname',    'r',    InputOption::VALUE_REQUIRED,  "Realm name")
            ->addOption('email',        'm',    InputOption::VALUE_REQUIRED,  "Mail address")
            ->addOption('password',     'p',    InputOption::VALUE_REQUIRED,  "Password")
            ->addOption('gender',       'g',    InputOption::VALUE_REQUIRED,  'Gender <comment>(default: "male")</comment>')
            ->addOption('race',         'ra',   InputOption::VALUE_REQUIRED,  'Race <comment>(default: "humans")</comment>')
            ->addOption('locale',       'l',    InputOption::VALUE_REQUIRED,  'Locale <comment>(default: "fr")</comment>')
            ->setDescription('Create a user')
            ->setHelp('This command creates a new user.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username   = $input->getOption('username');
        $realmname  = $input->getOption('realmname');
        $email      = $input->getOption('email');
        $password   = $input->getOption('password');
        $gender     = $input->getOption('gender');
        $race       = $input->getOption('race');
        $locale     = $input->getOption('locale');

        $requiredOptions = array('username', 'realmname', 'email', 'password');
        foreach($requiredOptions as $option) {
            if(empty($$option)) {
                $output->writeln("<error>No ".$option." given.</error>");
                $output->writeln("<info>".$this->getSynopsis()."</info>");
                return;
            }
        }

        // We use the FOSBundle
        $manager = $this->getContainer()->get('fos_user.user_manager');

        $user = $manager->createUser();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $user->setSuperAdmin(false);
        $user->setConfirmationToken(null);
        $user->setRegistrationRealmName($realmname);
        $user->setRegistrationRace(in_array($race, $this->getLLDC()['computed-param']['races']) ? $race : 'humans');
        $user->setRegistrationIdGuild(0);
        $user->setRegistrationGender(in_array($gender, array('male', 'female')) ? $gender : 'male');
        $user->setLocale(in_array($locale, array('fr', 'frRP')) ? $locale : 'fr');
        $user->setDateLastAction(new \Datetime());

        $manager->updateUser($user);

        $service = $this->getContainer()->get('lldc.user');
        $service->createUser($user);
        $output->writeln("<info>User n°".$user->getId()." created successfully.</info>");

        $this->end($output);
    }
}
