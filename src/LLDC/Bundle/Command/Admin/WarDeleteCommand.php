<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Admin
 */
namespace LLDC\Bundle\Command\Admin;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * This command deletes a war.
 * * Usage : <b>php app/console lldc:admin:war:delete --war-id=<ID></b>
 */
class WarDeleteCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:admin:war:delete')
            ->addOption('war-id', null, InputOption::VALUE_REQUIRED,  "War's ID")
            ->addOption('reason', null, InputOption::VALUE_OPTIONAL,  "Reason", "Technical issue, war deleted by an admin.")
            ->setDescription('Delete a war')
            ->setHelp('This command completly deletes a war from the database.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $warId = $input->getOption('war-id');
        $reason = $input->getOption('reason');
        $output->writeln("<comment>war n°".$warId." will be deleted...</comment>");

        $war = $this->getRepository('LLDCBundle:War')->findOneById($warId);
        if($war==null) {
            $output->writeln("<error>No war found for the id ".$warId."</error>");
        }
        else {
            $troops = $this->getRepository('LLDCBundle:Troop')->findByWar($war);
            foreach($troops as $troop) {
                $troop->setWar(null);
            }

            foreach($war->getAttackers() as $attacker) {
                $war->removeAttacker($attacker);
            }
            foreach($war->getDefenders() as $defender) {
                $war->removeDefender($defender);
            }
            $this->getManager()->remove($war);

            $this->getManager()->flush();
            $output->writeln("<info>War n°".$warId." deleted successfully.</info>");
        }

        $this->end($output);
    }
}
