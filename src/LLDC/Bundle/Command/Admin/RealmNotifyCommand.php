<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Admin
 */
namespace LLDC\Bundle\Command\Admin;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

use FOS\UserBundle\Util\UserManipulator;
use FOS\UserBundle\Util\Canonicalizer;

use LLDC\Bundle\Entity\User;

/**
 * This command notifies a realm.
 * * Usage : <b>php app/console lldc:admin:realm:notify </b>
 */
class RealmNotifyCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:admin:realm:notify')
            ->setDescription('Sends a notification to a realm')
            ->addOption('realm-id', 'r', InputOption::VALUE_REQUIRED,  "The realm")
            ->addOption('message', 'm', InputOption::VALUE_REQUIRED,  "The message to send")
            ->addOption('type', 't', InputOption::VALUE_REQUIRED,  "The type of the message")
            ->addOption('path', 'p', InputOption::VALUE_REQUIRED,  "The redirection route")
            ->setHelp('This command notifies a realm.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $realmId = $input->getOption('realm-id');
        $message = $input->getOption('message');
        $type = $input->getOption('type');
        $path = $input->getOption('path');

        $realm = $this->getRepository('LLDCBundle:Realm')->findOneById($realmId);
        if($realm==null) {
            $output->writeln("<error>No realm found for the id ".$realmId."</error>");
        }
        else {
            $type = isSet($type) ? $type : null;
            $service = $this->getContainer()->get('lldc.realm');
            $service->notify($realm, $message, $type, $path);
            $output->writeln("<info>Realm n°".$realm->getId()." notified successfully.</info>");
        }

        $this->getManager()->flush();
        $this->end($output);
    }
}
