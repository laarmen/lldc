<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Realm
 */
namespace LLDC\Bundle\Command\Realm;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * This command allows you to merge a troop.
 * * Usage : <b>php app/console lldc:realm:troop:merge</b>
 */
class TroopMergeCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:realm:troop:merge')
            ->setDescription('Utility command acting on the troops')
            ->setHelp('This command allows you to merge a troop.')
            ->addOption('troop-to-merge-id', 'm', InputOption::VALUE_REQUIRED, "Troop to merge id")
            ->addOption('troop-id', 't', InputOption::VALUE_REQUIRED, "Troop to merge in id")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $troopToMergeId = $input->getOption('troop-to-merge-id');
        $troopId = $input->getOption('troop-id');

        if(empty($troopToMergeId) || empty($troopId) || $troopToMergeId === $troopId) {
            $output->writeln("<info>".$this->getSynopsis()."</info>");
            return;
        }

        $troopToMerge = $this->getRepository('LLDCBundle:Troop')->findOneById($troopToMergeId);
        $troop = $this->getRepository('LLDCBundle:Troop')->findOneById($troopId);

        if(is_null($troopToMerge)) {
            $output->writeln("<error>The troop n°".$troopToMergeId." doesn't exist.</error>");
            return;
        }
        if(is_null($troop)) {
            $output->writeln("<error>The troop n°".$troop." doesn't exist.</error>");
            return;
        }

        $service = $this->getContainer()->get('lldc.troop');
        $service->merge($troopToMerge, $troop);

        $this->getManager()->flush();

        $output->writeln("<info>The troop n°".$troopToMergeId." has been merged into the troop n°".$troopId.".</info>");
        $this->end($output);
    }
}
