<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Realm
 */
namespace LLDC\Bundle\Command\Realm;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This command produces food, wood, gold and rock for every players.
 * * Usage : <b>php app/console lldc:realm:resource:produce</b>
 *
 * <b>Warning : Don't run this command yourself, unless you need it for testing purpose</b>
 */
class RealmResourceProduceCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:realm:resource:produce')
            ->setDescription('Produces resources for all actives realms')
            ->setHelp('This command produces food, wood, gold and rock for every players.'."\n\n".
            '<error>Don\'t run this command yourself, unless you need it for testing purpose</error>')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Fetching parameters
        $lldc = $this->getLLDC();

        // Fetching the games to initialize realm settings
        $games = $this->getRepository('LLDCBundle:Game')->findAll();

        // Production speed, the same for everyone, used in the log() function
        $baseFoodProdSpeed = $lldc['global']['resources']['production_speed']['food'];
        $baseWoodProdSpeed = $lldc['global']['resources']['production_speed']['wood'];
        $baseRockProdSpeed = $lldc['global']['resources']['production_speed']['rock'];
        $baseGoldProdSpeed = $lldc['global']['resources']['production_speed']['gold'];

        $output->writeln(
            '<comment>Base speed : [food] '.$baseFoodProdSpeed.' - [wood] '.$baseWoodProdSpeed.' - [rock] '.$baseRockProdSpeed.' - [gold] '.$baseGoldProdSpeed.'</comment>'
        );

        foreach($games as $game) {
            $output->writeln('<comment>'.$game->getLabel()." :</comment>");

            $gameFoodProdRate = $game->getProductionRateFood();
            $gameWoodProdRate = $game->getProductionRateWood();
            $gameRockProdRate = $game->getProductionRateRock();
            $gameGoldProdRate = $game->getProductionRateGold();

            $output->writeln(
                '<comment>Prod rate : [food] '.$gameFoodProdRate.' - [wood] '.$gameWoodProdRate.' - [rock] '.$gameRockProdRate.' - [gold] '.$gameGoldProdRate.'</comment>'
            );

            $realms = $this->getRepository('LLDCBundle:Realm')->findByGame($game->getId());

            foreach($realms as $realm) {

                $peon = $this->getRepository('LLDCBundle:Troop')->findSumAvailableByRealmAndTypeAsTotal($realm, 'peon')['total'];

                $foodProdPercent = $realm->getProduction()->getFoodProdPercent();
                $woodProdPercent = $realm->getProduction()->getWoodProdPercent();
                $rockProdPercent = $realm->getProduction()->getRockProdPercent();
                $goldProdPercent = $realm->getProduction()->getGoldProdPercent();

                $peonOnFood = $peon * $foodProdPercent / 100;
                $peonOnWood = $peon * $woodProdPercent / 100;
                $peonOnRock = $peon * $rockProdPercent / 100;
                $peonOnGold = $peon * $goldProdPercent / 100;

                // Can be improved by storing the 5 races production in an array before the foreachs
                $raceProduction = $lldc['races'][$realm->getCharacter()->getRace()->getLabel()]['production_rate'];


                $output->writeln('<info>'.$realm->getPlace()->getName().' :</info>');
                $output->writeln($peon.' peons ([food] '.$peonOnFood.' - [wood] '.$peonOnWood.' - [rock] '.$peonOnRock.' - [gold] '.$peonOnGold.')');

                $output->writeln(
                    '<comment>Race prod rate : [food] '.$raceProduction['food'].' - [wood] '.$raceProduction['wood'].' - [rock] '.$raceProduction['rock'].' - [gold] '.$raceProduction['gold'].')</comment>'."\n"
                );

                $foodProduction = (($peonOnFood*60)  / (log($peon, $baseFoodProdSpeed) * 100)) * $gameFoodProdRate * $raceProduction['food'];
                $woodProduction = (($peonOnWood*60)  / (log($peon, $baseWoodProdSpeed) * 100)) * $gameWoodProdRate * $raceProduction['wood'];
                $rockProduction = (($peonOnRock*60)  / (log($peon, $baseRockProdSpeed) * 100)) * $gameRockProdRate * $raceProduction['rock'];
                $goldProduction = (($peonOnGold*60)  / (log($peon, $baseGoldProdSpeed) * 100)) * $gameGoldProdRate * $raceProduction['gold'];

                $output->writeln('Production : [food] '.$foodProduction.' - [wood] '.$woodProduction.' - [rock] '.$rockProduction.' - [gold] '.$goldProduction.'');

                $realm->getResource()->setFood($realm->getResource()->getFood()+$foodProduction);
                $realm->getResource()->setWood($realm->getResource()->getWood()+$woodProduction);
                $realm->getResource()->setRock($realm->getResource()->getRock()+$rockProduction);
                $realm->getResource()->setGold($realm->getResource()->getGold()+$goldProduction);
                $this->getContainer()->get('lldc.ranking')->updatePoints($realm);
            }
            $this->getContainer()->get('lldc.ranking')->updatePointsPosition($game);
        }

        $this->getManager()->flush();

        $this->end($output);
    }
}
