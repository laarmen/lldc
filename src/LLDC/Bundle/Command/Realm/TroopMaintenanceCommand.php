<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Realm
 */
namespace LLDC\Bundle\Command\Realm;

use LLDC\Bundle\Command\LLDCCommand;
use LLDC\Bundle\LLDCException;
use LLDC\Bundle\DependencyInjection\Realm\ResourcesService;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This command retrieves the cost of units maintenance.
 * * Usage : <b>php app/console lldc:realm:troop:maintenance</b>
 *
 * <b>Warning : Don't run this command yourself, unless you need it for testing purpose</b>
 */
class TroopMaintenanceCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:realm:troop:maintenance')
            ->setDescription('Retrieve the cost of units maintenance for all actives realms')
            ->setHelp('This command takes the cost of units maintenance. If the user doesn\'t have the resources needed, his morale falls down.'."\n\n".
            '<error>Don\'t run this command yourself, unless you need it for testing purpose</error>')
        ;
    }

    /**
     * Execute the lldc:realm:troop:maintenance command.
     * TODO : It doesn't take care of the XP ! (no XP cost / no morale loss if more XP needed)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lldc = $this->getLLDC();

        // Get the services
        $troopService = $this->getContainer()->get('lldc.troop');
        $resourcesService = $this->getContainer()->get('lldc.resources');

        $globalConf = '';
        foreach($lldc['units'] as $race => $racesConf) {
            $globalConf .= "\n<comment>".$race."</comment>\n";
            foreach($racesConf as $type => $unitsConf) {
                $globalConf .= " <info>".$type."</info>";
                foreach($unitsConf['cost']['maintenance'] as $resourceType => $value) {
                    $globalConf .= ' ['.$resourceType.'] '.$value;
                }
            }
        }
        $output->writeln($globalConf);

        // Fetching the games
        $games = $this->getRepository('LLDCBundle:Game')->findAll();

        foreach($games as $game) {
            $output->writeln('<comment>'.$game->getLabel()." :</comment>");

            $gameFactor = $game->getUnitsMaintenanceFactor();
            $gameDuringInternalWar = $game->getUnitsMaintenanceDuringInternalWar();
            $gameDuringExternalWar = $game->getUnitsMaintenanceDuringExternalWar();
            $gameMoraleLoss = $game->getUnitsMaintenanceMoraleLoss();

            $output->writeln(
                '<comment>Game conf : [factor] '.$gameFactor.' - [in-war] '.$gameDuringInternalWar.' - [ex-war] '.$gameDuringExternalWar.' - [morale-loss] '.$gameMoraleLoss.'</comment>'
            );

            $realms = $this->getRepository('LLDCBundle:Realm')->findByGame($game->getId());

            foreach($realms as $realm) {
                try {
                    $resourcesService->take(
                        $troopService->getMaintenanceCost(
                            $realm,
                            $gameFactor,
                            $gameDuringInternalWar,
                            $gameDuringExternalWar,
                            $lldc['units'][$realm->getCharacter()->getRace()->getLabel()]
                    ), $realm);
                }
                catch(LLDCException $e) {
                    if($e->getCode()==LLDCException::NOT_ENOUGH_MADRENS) {
                        $morale = $resourcesService->changeMorale(-$gameMoraleLoss, $realm, ResourcesService::MORALE_PERCENTAGE);
                        $output->writeln('<error>Realm n°'.$realm->getId().' : '.$e->getMessage().'... Morale set to '.$morale.'</error>');
                    }
                }
            }
        }

        $this->getManager()->flush();

        $this->end($output);
    }
}
