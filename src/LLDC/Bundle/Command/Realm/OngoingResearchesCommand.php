<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Realm
 */
namespace LLDC\Bundle\Command\Realm;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use LLDC\Bundle\Entity\RealmResearch;

/**
 * This command ends ongoing researches.
 * * Usage : <b>php app/console lldc:realm:ongoing:researches</b>
 *
 * <b>Warning : Don't run this command yourself, unless you need it for testing purpose</b>
 */
class OngoingResearchesCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:realm:ongoing:researches')
            ->setDescription('Ends ongoing researches for all actives realms')
            ->setHelp('This command ends ongoing researches for every players.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getManager();

        // Fetching parameters
        $lldc = $this->getLLDC();

        // Fetching the games to initialize realm settings
        $games = $this->getRepository('LLDCBundle:Game')->findAll();

        foreach($games as $game) {
            $output->writeln('<comment>'.$game->getLabel()." :</comment>");

            $realms = $this->getRepository('LLDCBundle:Realm')->findByGame($game->getId());
            $date = new \DateTime();

            foreach($realms as $realm) {
                $output->writeln('<info>'.$realm->getPlace()->getName().' :</info>');
                
                $research = $realm->getOngoingResearch();
                if(!is_null($research)) {
                    $output->writeln("\t".$research->getResearch()." ongoing");
                    if($research->getDateEnd() <= $date) {
                        $realmResearch = new RealmResearch();
                        $realmResearch->setRealm($realm);
                        $realmResearch->setResearch($research->getResearch());
                        $realmResearch->setDate($date);
                        
                        $doctrine->persist($realmResearch);
                        $doctrine->remove($research);
                        $output->writeln("\t|--> It ends now");
                    }
                    else {
                        $output->writeln("\t|--> It'll later (".$research->getDateEnd()->format(\DateTime::RSS).")");
                    }
                }
            }
        }

        $doctrine->flush();

        $this->end($output);
    }
}
