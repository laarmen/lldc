<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Realm
 */
namespace LLDC\Bundle\Command\Realm;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * This command allows you to split a troop.
 * * Usage : <b>php app/console lldc:realm:troop:split</b>
 */
class TroopSplitCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:realm:troop:split')
            ->setDescription('Utility command acting on the troops')
            ->setHelp('This command allows you to split a troop.')
            ->addOption('troop-id', 't', InputOption::VALUE_REQUIRED, "Troop id")
            ->addOption('ratio', 'r', InputOption::VALUE_OPTIONAL, "Ratio <comment>(default: 0.5)</comment>")
            ->addOption('parity', 'p', InputOption::VALUE_OPTIONAL, "Parity <comment>(default: 1)</comment>")
            ->addOption('type', 'ty', InputOption::VALUE_OPTIONAL, "Type <comment>(default: troop's type)</comment>")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Fetching parameters
        $lldc = $this->getLLDC();

        $troopId = $input->getOption('troop-id');
        $ratio = $input->getOption('ratio');
        $parity = $input->getOption('parity');
        $type = $input->getOption('type');

		if(empty($troopId)) {
            $output->writeln("<info>".$this->getSynopsis()."</info>");
            return;
		}

        $troop = $this->getRepository('LLDCBundle:Troop')->findOneById($troopId);

        $service = $this->getContainer()->get('lldc.troop');
        $service->split($troop, $ratio, $parity, $type);

        $this->getManager()->flush();

        $this->end($output);
    }
}
