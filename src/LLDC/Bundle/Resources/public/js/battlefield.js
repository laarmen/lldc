/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/* global d3 */

// This JS code is partially licensed under the 3-clause BSD license, see
// https://github.com/d3/d3.github.com/blob/aba0ce61b5b3ed70c3d5c8462fdad706e1f415db/LICENSE


var httpcon = new XMLHttpRequest();
var troopImages = {};
var Troop = function (jsonObj) {
    for (var key in jsonObj) {
        this[key] = jsonObj[key];
    }
    tImage = troopImages[this.full_type];
    if (tImage === undefined) {
        tImage = new Image;
        tImage.src = "/bundles/lldc/images/units/"+this.full_type+".png";
        troopImages[this.full_type] = tImage;
    }
    this.image = tImage;
};

var Square = function (jsonObj, troops) {
    this.id = jsonObj.id;
    this.groundType = jsonObj.groundType;
    this.x = jsonObj.x;
    this.y = jsonObj.y;
    this.z = jsonObj.z;
    this.off_q = jsonObj.x + (jsonObj.z - (jsonObj.z & 1)) / 2;
    this.off_r = jsonObj.z;
    if (jsonObj.troop !== undefined) {
        this.troop = troops[jsonObj.troop];
    }
    this.z_level = 0;
};

var Battlefield = function (bfId, containerID, drawingAreaID) {
    var data;
    httpcon.open("GET", Routing.generate('json_battlefield', { id: bfId }), false);
    httpcon.send();
    var data = JSON.parse(httpcon.responseText);
    this.drawingAreaID = drawingAreaID;
    this.containerID = containerID;
    this.id = data.id;
    this.troops = {};
    data.troops.forEach(function(t) {
        var troop = new Troop(t);
        this.troops[troop.id] = troop;
    }, this);
    this.squares = data.squares.map(function (s) {
        return new Square(s, this.troops);
    }, this);

    var min_q;
    var max_q;
    var min_r;
    var max_r;
    this.squares.forEach(function (square, i) {
        if (!(min_q < square.off_q)) {
            min_q = square.off_q;
        }
        if (!(min_r < square.off_r)) {
            min_r = square.off_r;
        }
        if (!(max_q > square.off_q)) {
            max_q = square.off_q;
        }
        if (!(max_r > square.off_r)) {
            max_r = square.off_r;
        }
    });

    var grid = [];
    // Fill up the squareGrid
    this.squares.forEach(function (square, i) {
        var x = square.off_q - min_q;
        var y = square.off_r - min_r;
        // Shift the whole thing towards south-east because of border problems at north and west.
        // If you don't understand, ask Eremanth.
        if (y % 2 === 1) {
            x += 1;
        }
        y += 1;

        if (grid[x] === undefined) {
            grid[x] = [];
        }
        grid[x][y] = square;
    });
    // TODO: dynamic radius?
    this.squareGrid = grid;
    this.radius = 30;
    this.depth = 6;
    this.height = Math.round(this.radius * Math.sqrt(3) * (max_q - min_q)) + 2 * this.radius;
    // Make things a bit wider than needed to display the highlight on the edge squares.
    this.width = Math.round(this.radius * Math.sqrt(3) * (max_r - min_r + 1)) + 2 * this.radius;
    this.imageWidth = this.radius * 1.732;
    this.imageHeight = 2*this.radius;

    var container = d3.select("#" + containerID)[0][0];
    container.setAttribute("style", "height: " + this.height + "px; width: " + this.width + "px;");

    // Initialization of a shitload of variables used to draw in and stuff.
    // Also, "varification" of some variables 'cause JS is dumb and cannot
    // look for them in this when in a closure. Stupid language...
    var squareGrid = this.squareGrid;
    var radius = this.radius;
    var imageWidth = this.imageWidth;
    var imageHeight = this.imageHeight;


    var hexbin = d3.hexbin().radius(radius);

    var battlefieldDisplay = d3.select("#" + drawingAreaID);
    var canvas = battlefieldDisplay.append("canvas").attr("height", this.height);
    var context = canvas.node().getContext("2d");
    var svg = battlefieldDisplay.append("svg").attr("height", this.height);
    var mesh = svg.append("path").attr("class", "battlefield-mesh");
    var anchor = svg.append("g").attr("class", "battlefield-anchor").selectAll("a");

    var graphic = battlefieldDisplay.selectAll("svg,canvas");

    var image = new Image;
    image.src = "/bundles/lldc/images/bf_bg.jpg";
    image.onload = resized;

    d3.select(window).on("resize", resized);

    function drawImage(s) {
        context.save();
        context.beginPath();
        context.moveTo(0, -radius);

        // Draw an hexagon.
        for (var i = 1; i < 6; ++i) {
            var angle = i * Math.PI / 3,
                x = Math.sin(angle) * radius,
                y = -Math.cos(angle) * radius;
            context.lineTo(x, y);
        }
        context.clip();

        // Fill it up with an image.
        var constant;
        // TODO: properly handle all terrain types.
        if (s.groundType === "grass") {
            constant = 5;
        } else {
            constant = 1;
        }
        context.drawImage(image,
            imageWidth * constant, imageHeight * constant,
            imageWidth, imageHeight,
            -imageWidth / 2, -imageHeight / 2,
            imageWidth, imageHeight);
        if (s.troop !== undefined) {
            context.drawImage(s.troop.image,
                -imageWidth / 2, -imageHeight / 2,
                imageWidth, imageHeight);
        }
        context.restore();
    }
    ;

    var squares = this.squares;
    function resized() {
        var container = d3.select("#" + containerID)[0][0];
        var displayWidth = container.scrollWidth;
        var displayHeight = container.scrollHeight;
        var centers = hexbin.size([displayWidth, displayHeight]).centers();

        graphic
            .attr("width", displayWidth)
            .attr("height", displayHeight);

        centers.forEach(function (center, i) {
            var square;
            var squareColumn = squareGrid[center.i];
            // We have to check for invalid values in the column too.
            if (squareColumn !== undefined) {
                square = squareColumn[center.j];
            }
            // These maths must be right, I guess. I dunno, I don't understand what's going on.
            center.j = Math.round(center[1] / (radius * 1.5));
            center.i = Math.round((center[0] - (center.j & 1) * radius * Math.sin(Math.PI / 3)) / (radius * 2 * Math.sin(Math.PI / 3)));
            var square;
            var squareColumn = squareGrid[center.i];
            // We have to check for invalid values in the column too.
            if (squareColumn !== undefined) {
                square = squareColumn[center.j];
            }
            context.save();
            // Adding some more radius helps keeping things centered. So it seems.
            // I blame the +1 from the beginning. I think.
            context.translate(Math.round(center[0]) + radius, Math.round(center[1]) + radius);

            if (square !== undefined) { // We're in the grid.
                square.center = center;
                drawImage(square);
            }
            context.restore();
        });

        mesh.attr("d", hexbin.mesh);

        anchor = anchor.data(squares, function (square) {
            return square.center.i + "," + square.center.j;
        });

        anchor.exit().remove();

        anchor.enter().append("a")
            .attr("xlink:title", function (square) {
                return "(" + square.x + "," + square.y + "," + square.z + ")";
            })
            .attr("class", function (square) {
                return square.groundType;
            })
            .append("path")
            .attr("d", hexbin.hexagon());

        // Placing the link grid.
        // Here, we add some more radius to center the link grid.
        // The reason? I tried it, it worked. Why? Your guess is as good as mine.
        anchor
            .attr("transform", function (square) {
                square.center[0] += radius;
                square.center[1] += radius;
                return "translate(" + square.center + ")";
            });
    }
    resized();
    this.sortAnchors();
};

// Sorting the anchors by order of priority
// The last one will be drawned last, which is a very important info when drawing
// overlapping paths with different colors.
Battlefield.prototype.sortAnchors = function () {
    var anchor = d3.select("#" + this.drawingAreaID).select("svg").select("g").selectAll("a");
    anchor.sort(function (s1, s2) {
        if (s1.z_level < s2.z_level)
            return -1;
        if (s1.z_level > s2.z_level)
            return 1;
        return 0;
    });
};

Battlefield.prototype.eraseActionables = function () {
    var anchor = d3.select("#" + this.drawingAreaID).select("svg").select("g").selectAll("a");
    anchor.filter(".actionable").on(".click", null).classed("actionable", false);
};

Battlefield.prototype.eraseSelected = function () {
    var anchor = d3.select("#" + this.drawingAreaID).select("svg").select("g").selectAll("a");
    anchor.filter(".selected").on(".click", null).classed("selected", false);
};

// callback takes one parameter (the square)
Battlefield.prototype.makeSquaresActionables = function (square_ids, callback)
{
    var areaID = this.drawingAreaID;
    var bf = this;
    var anchor = d3.select("#" + areaID).select("svg").select("g").selectAll("a");
    anchor.filter(".actionable").classed("actionable", false);
    anchor.filter(function (square, i) {
        if (square_ids.indexOf(square.id) >= 0) {
            square.z_level = 1;
            return true;
        }
        return false;
    })
        .classed("actionable", true)
        .on("click", function (square, i)
        {
            this.classList.add("selected");
            square.z_level = 2;
            bf.sortAnchors();
            callback(square);
        });
    this.sortAnchors();
};
