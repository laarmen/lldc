/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

$(document).ready(function () {
    $('.lldccode').each(function (index) {
        var objectToParse = $(this);
        objectToParse.attr('content', objectToParse.html());
        objectToParse.html('Chargement');
        // Asynchronous parsing request
        setTimeout(function () {
            objectToParse.html(lldccode(objectToParse.attr('content')));
        }, 0, objectToParse);
    });
});

function make_html_simple_markup(markup) {
    return function (string, options) {
        return "<" + markup + ">" + string + "</" + markup + ">";
    };
}

function lldccode(string) {
    string = string.replace(/\n/ig, '<br />');
    string = string.replace(
        /(((https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])|((www)[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])|\[link=(.*?)](.*?)\[\/link])/ig,
        link
        );
    tmp = parse(0, string, null);
    return tmp[0] + string.substr(tmp[1]);
}

var artifacts = {
    'acres': '<img src="/bundles/lldc/images/icons/acres.png" alt="acres" />',
    'amnerian_cross_green': '<img src="/bundles/lldc/images/icons/amnerian_cross_green.png" alt="amnerian_cross_green" />',
    'amnerian_cross_grey': '<img src="/bundles/lldc/images/icons/amnerian_cross_grey.png" alt="amnerian_cross_grey" />',
    'amnerian_cross_red': '<img src="/bundles/lldc/images/icons/amnerian_cross_red.png" alt="amnerian_cross_red" />',
    'arrow_bottom': '<img src="/bundles/lldc/images/icons/arrow_bottom.png" alt="arrow_bottom" />',
    'arrow_left': '<img src="/bundles/lldc/images/icons/arrow_left.png" alt="arrow_left" />',
    'arrow_left_end': '<img src="/bundles/lldc/images/icons/arrow_left_end.png" alt="arrow_left_end" />',
    'arrow_left_grey': '<img src="/bundles/lldc/images/icons/arrow_left_grey.png" alt="arrow_left_grey" />',
    'arrow_right': '<img src="/bundles/lldc/images/icons/arrow_right.png" alt="arrow_right" />',
    'arrow_right_end': '<img src="/bundles/lldc/images/icons/arrow_right_end.png" alt="arrow_right_end" />',
    'arrow_right_grey': '<img src="/bundles/lldc/images/icons/arrow_right_grey.png" alt="arrow_right_grey" />',
    'arrow_top': '<img src="/bundles/lldc/images/icons/arrow_top.png" alt="arrow_top" />',
    'bag': '<img src="/bundles/lldc/images/icons/bag.png" alt="bag" />',
    'building': '<img src="/bundles/lldc/images/icons/building.png" alt="building" />',
    'dice': '<img src="/bundles/lldc/images/icons/dice.png" alt="dice" />',
    'edit': '<img src="/bundles/lldc/images/icons/edit.png" alt="edit" />',
    'flask': '<img src="/bundles/lldc/images/icons/flask.png" alt="flask" />',
    'food': '<img src="/bundles/lldc/images/icons/food.png" alt="food" />',
    'gender_female': '<img src="/bundles/lldc/images/icons/gender_female.png" alt="gender_female" />',
    'gender_male': '<img src="/bundles/lldc/images/icons/gender_male.png" alt="gender_male" />',
    'gold': '<img src="/bundles/lldc/images/icons/gold.png" alt="gold" />',
    'help': '<img src="/bundles/lldc/images/icons/help.png" alt="help" />',
    'inkwell': '<img src="/bundles/lldc/images/icons/inkwell.png" alt="inkwell" />',
    'inkwell_color_blind': '<img src="/bundles/lldc/images/icons/inkwell_color_blind.png" alt="inkwell_color_blind" />',
    'inkwell_red': '<img src="/bundles/lldc/images/icons/inkwell_red.png" alt="inkwell_red" />',
    'inkwell_red_color_blind': '<img src="/bundles/lldc/images/icons/inkwell_red_color_blind.png" alt="inkwell_red_color_blind" />',
    'locked': '<img src="/bundles/lldc/images/icons/locked.png" alt="locked" />',
    'locked_grey': '<img src="/bundles/lldc/images/icons/locked_grey.png" alt="locked_grey" />',
    'locked_grey_color_blind': '<img src="/bundles/lldc/images/icons/locked_grey_color_blind.png" alt="locked_grey_color_blind" />',
    'locked_red': '<img src="/bundles/lldc/images/icons/locked_red.png" alt="locked_red" />',
    'locked_red_color_blind': '<img src="/bundles/lldc/images/icons/locked_red_color_blind.png" alt="locked_red_color_blind" />',
    'madrens': '<img src="/bundles/lldc/images/icons/madrens.png" alt="madrens" />',
    'morale': '<img src="/bundles/lldc/images/icons/morale.png" alt="morale" />',
    'rock': '<img src="/bundles/lldc/images/icons/rock.png" alt="rock" />',
    'time': '<img src="/bundles/lldc/images/icons/time.png" alt="time" />',
    'torture': '<img src="/bundles/lldc/images/icons/torture.png" alt="torture" />',
    'weather': '<img src="/bundles/lldc/images/icons/weather.png" alt="weather" />',
    'wood': '<img src="/bundles/lldc/images/icons/wood.png" alt="wood" />',
    'xp': '<img src="/bundles/lldc/images/icons/xp.png" alt="xp" />',
    'nbsp': '&nbsp;'
};

var markups = {
    'b': make_html_simple_markup('b'),
    'i': make_html_simple_markup('i'),
    'u': make_html_simple_markup('u'),
    'strike': make_html_simple_markup('strike'),
    'over': function (str, opt) {
        return '<span title="' + opt.def + '">' + str + '</span>';
    },
    'color': function (str, opt) {
        return '<span class="' + opt.def + '">' + str + '</span>';
    },
    'size': function (str, opt) {
        return '<span style="font-size:' + Math.max(Math.min(30, opt.def), 8) + 'px">' + str + '</span>';
    },
    'align': function (str, opt) {
        return '<div align="' + ((["center", "right"].indexOf(opt.def) >= 0) ? opt.def : 'left') + '">' + str + '</div>';
    },
    'margin': function (str, opt) {
        var val = Math.max(Math.min(100, opt.def), 8);
        return '<div style="margin: 0px ' + val + 'px 0px ' + val + 'px;">' + str + '</div>';
    },
    'quote': function (str, opt) {
        return '<fieldset><legend>Citation' + (("def" in opt) ? ' de ' + opt.def : '') + '</legend>' + str + '</fieldset>';
    }
};

var verbatim_markup = "lldccode";
function verbatim(string) {
    return string;
}
;

function add_str(str) {
    this.val += str;
}

function parse(index_beg, string, markup_end) {
    var lexems = [];
    var beg = index_beg;
    var res = {'val': ''};
    var match = null;

    // We have to make it a variable since it mutates every time it is used -_-'
    var expression = /\[(?:([a-z_]+)(?:(?:=([^\]]+))|((?: +[a-z_]+=.+?)+?))?|(?:\/([a-z_]+)))\]|:([a-z_]+):/ig;
    expression.lastIndex = beg;

    // Take elements one by one
    while ((match = expression.exec(string)) !== null) {
        lexems.push(string.substring(beg, match.index));
        beg = match.index + match[0].length;

        // If we encounter a closing markup
        if (match[4] !== null) {
            if (markup_end !== null && markup_end === match[4]) {
                lexems.forEach(add_str, res);
                return [res.val, beg];
            } else { // Simply ignore unexpected markup
                lexems.push(match[0]);
                continue;
            }
        }

        var artifact = match[5]; // The one-time replacement, no closing necessary.
        if (artifact !== null) {
            artifact = artifacts[artifact];
            lexems.push((artifact !== null) ? artifact : match[0]);
            continue;
        }

        var markup = match[1];
        if (markup === verbatim_markup) {
            var verb_end = "[/" + verbatim_markup + "]";
            var text = string.substring(beg, string.indexOf(verb_end, beg));
            beg += text.length + verb_end.length;
            expression.lastIndex = beg;
            lexems.push(verbatim(text));
            continue;
        }

        var markup_proc = markups[markup];
        if (markup_proc === null) {
            lexems.push(match[0]);
            continue;
        }
        var options = {}; // Option handling
        if (match[2] !== null) {  // [a=12] => options.def == 12
            options["def"] = match[2];
        } else if (match[3] !== null) { // [a b=12 c=13] => options.b == 12 && options.c == 13
            match[3].trim().split(' ').forEach(function (str) {
                if (str.length <= 1)
                    return;
                var tmp = str.split("=");
                this[tmp[0]] = tmp[1];
            }, options);
        }

        var nested_call = parse(beg, string, markup);
        if (nested_call[1] === null) {
            lexems.push(match[0]);
            lexems = lexems.concat(markup_proc(nested_call[0], options));
        } else {
            lexems = lexems.concat(markup_proc(nested_call[0], options));
            beg = nested_call[1];
            expression.lastIndex = beg;
        }
    }
    lexems.forEach(add_str, res);
    if (markup_end !== null) {
        res.val += string.substring(beg);
        beg = null;
    }
    return [res.val, beg];
}

function link(dummy, url, dummy2, protocol) {
    var href = '';
    if (typeof protocol !== "undefined") {
        href = url;
    }
    else {
        href = 'http://' + url;
    }
    return '<a href="' + href + '" target="_blank">' + url + '</a>';
}
