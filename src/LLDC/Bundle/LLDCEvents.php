<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle
 */
namespace LLDC\Bundle;

/**
 * Contains all events thrown in the LLDC Bundle
 */
final class LLDCEvents
{
    //------
    // BUILDINGS
    //------
    /**
     * The BUILDING_BUILT event occurs when the user obtains new buildings
     */
    const BUILDING_BUILT = 101;
    /**
     * The BUILDING_LOST event occurs when the user loses buildings
     */
    const BUILDING_LOST = 102;
    /**
     * The BUILDING_DESTROYED event occurs when the user destroys is own buildings
     */
    const BUILDING_DESTROYED = 103;
    /**
     * The ENEMY_BUILDING_DESTROYED event occurs when the user destroys enemy buildings
     */
    const ENEMY_BUILDING_DESTROYED = 104;

    //------
    // TROOPS
    //------
    /**
     * The ACADEMY_UNIT_BOUGHT event occurs when the user buy units in the academy
     */
    const ACADEMY_UNIT_BOUGHT = 201;
    /**
     * The TROOP_SPLIT event occurs when a troop is split
     */
    const TROOP_SPLIT = 202;
    /**
     * The WAR_TROOP_SPLIT event occurs when a troop is split during a war
     */
    const WAR_TROOP_SPLIT = 203;
    /**
     * The TROOP_TRIGGER_BEFORE_ATTACK event occurs before an attack (attacker side)
     */
    const TROOP_TRIGGER_BEFORE_ATTACK = 204;
    /**
     * The TROOP_TRIGGER_BEFORE_DEFEND event occurs before an attack (defender side)
     */
    const TROOP_TRIGGER_BEFORE_DEFEND = 205;
    /**
     * The TROOP_TRIGGER_AFTER_ATTACK event occurs after an attack (attacker side)
     */
    const TROOP_TRIGGER_AFTER_ATTACK = 206;
    /**
     * The TROOP_TRIGGER_AFTER_DEFEND event occurs after an attack (defender side)
     */
    const TROOP_TRIGGER_AFTER_DEFEND = 207;
    /**
     * The TROOP_TRIGGER_BEFORE_DIRECTION_CHANGE event occurs before changing direction on the battlefield
     */
    const TROOP_TRIGGER_BEFORE_DIRECTION_CHANGE = 208;
    /**
     * The TROOP_TRIGGER_AFTER_DIRECTION_CHANGE event occurs after changing direction on the battlefield
     */
    const TROOP_TRIGGER_AFTER_DIRECTION_CHANGE = 209;
    /**
     * The TROOP_TRIGGER_BEFORE_END_OF_TURN event occurs before the end of the turn
     */
    const TROOP_TRIGGER_BEFORE_END_OF_TURN = 210;
    /**
     * The TROOP_TRIGGER_AFTER_END_OF_TURN event occurs after the end of the turn
     */
    const TROOP_TRIGGER_AFTER_END_OF_TURN = 211;
    /**
     * The TROOP_TRIGGER_BEFORE_INJURED event occurs before a troop to get injured
     */
    const TROOP_TRIGGER_BEFORE_INJURED = 212;
    /**
     * The TROOP_TRIGGER_AFTER_INJURED event occurs after a troop to get injured
     */
    const TROOP_TRIGGER_AFTER_INJURED = 213;

}
